//
//  ycScrollview.m
//  无限轮播图
//
//  Created by cvicseks-mac1 on 16/3/1.
//  Copyright © 2016年 cvicseks-mac1. All rights reserved.
//

#define ycScrollViewWidth self.frame.size.width
#define ycScrollViewHeight self.frame.size.height
#define ycScrollViewY self.frame.origin.y

#import "ycScrollview.h"
@interface ycScrollview()
<UIScrollViewDelegate>
@property (nonatomic, retain) UIImageView *firstImageView;
@property (nonatomic, retain) UIImageView *sencondImageView;
@property (nonatomic, retain) UIImageView *threeImageView;
@property (nonatomic, retain) UIPageControl *pageControl;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, assign) BOOL isTimerUp;
@property (nonatomic, strong) void (^block)();
// 当前显示的图片
@property (nonatomic, assign) NSInteger imageArrayIndex;
@end
@implementation ycScrollview

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self initScrollView];
    }
    return self;
}

- (void)initScrollView
{
    self.bounces = NO; // 无弹动画效果
    self.pagingEnabled = YES;
    self.showsHorizontalScrollIndicator = NO;
    self.showsVerticalScrollIndicator = NO;
    self.contentOffset = CGPointMake(ycScrollViewWidth, 0);
    // scrollVIew加在一个已经加了UINavigationController和tabBarViewController 的ViewController里.如果scrollVIew的contentSize的高度是整个屏幕则会出现上下拖动的效果，把它高度改为0即可。有时候就算contentSize的高度改为0还是会出现这种情况，则再加上alwaysBounceVertical = NO;即可 还是不行就只能调节self.navigationController.navigationBar.translucent = NO;
    // 或者 self.automaticallyAdjustsScrollViewInsets = NO;
    self.contentSize = CGSizeMake(ycScrollViewWidth * 3, ycScrollViewHeight);
    self.delegate =self;
    
    _imageArrayIndex = 0;
    _isTimerUp = NO;
    
    _firstImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, ycScrollViewWidth, ycScrollViewHeight)];
    [self addSubview:_firstImageView];
    _sencondImageView = [[UIImageView alloc] initWithFrame:CGRectMake(ycScrollViewWidth, 0, ycScrollViewWidth, ycScrollViewHeight)];
    [self addSubview:_sencondImageView];
    _threeImageView = [[UIImageView alloc] initWithFrame:CGRectMake(ycScrollViewWidth*2, 0, ycScrollViewWidth, ycScrollViewHeight)];
    [self addSubview:_threeImageView];
}

- (void)setImageArray:(NSMutableArray *)imageArray
{
    [self initpageControlWithImageArray:imageArray];
    
    if (imageArray.count == 2) {
        NSString *firstimage = [imageArray firstObject];
        NSString *lastimage = [imageArray lastObject];
        _imageArray = [[NSMutableArray alloc] initWithArray:imageArray];
        [_imageArray addObject:firstimage];
        [_imageArray addObject:lastimage];
    }else if (imageArray.count == 1)
    {
        _firstImageView.image = [UIImage imageNamed:imageArray.firstObject];
        self.contentSize = CGSizeMake(ycScrollViewWidth, ycScrollViewHeight);
        return;
    }
    else
    {
        _imageArray = [[NSMutableArray alloc] initWithArray:imageArray];
    }
    // 第一种算法
//    NSString *image = [imageArray lastObject];
//    NSMutableArray *array = [[NSMutableArray alloc] initWithArray:imageArray];
//    [array removeLastObject];
//    [array insertObject:image atIndex:0];
//    _imageArray = [NSMutableArray arrayWithArray:array];
    
    // 第二种算法
    NSInteger firstIndex = 0;
    NSInteger threeIndex = 0;
    firstIndex = (_imageArrayIndex - 1 + _imageArray.count)%_imageArray.count;
    threeIndex = (_imageArrayIndex + 1)%_imageArray.count;
    
    _firstImageView.image = [UIImage imageNamed:_imageArray[firstIndex]];
    _sencondImageView.image = [UIImage imageNamed:_imageArray[_imageArrayIndex]];
    _threeImageView.image = [UIImage imageNamed:_imageArray[threeIndex]];
    if (!_isTimerUp) {
        [self setupTiemr];
    }
}

- (void)initpageControlWithImageArray:(NSMutableArray *)imageArray
{
    _pageControl = [[UIPageControl alloc] init];
    _pageControl.numberOfPages = imageArray.count;
    _pageControl.frame = CGRectMake((ycScrollViewWidth - 20*imageArray.count)/2, ycScrollViewY + ycScrollViewHeight -20, 20 * imageArray.count, 20);
    [self performSelector:@selector(addPageControl) withObject:nil afterDelay:0.1f];
}

- (void)addPageControl
{
    [[self superview] addSubview:_pageControl];
}

#pragma UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSInteger firstIndex = 0;
    NSInteger threeIndex = 0;
    // 向右滑动显示的是第二个iamgeview  == 0
    if(self.contentOffset.x < ycScrollViewWidth)
    {
//        _imageArrayIndex --;
//        //判断计算
//        if (_imageArrayIndex < 0) {
//            _imageArrayIndex = _imageArray.count - 1;
//            firstIndex = _imageArrayIndex - 1;
//             threeIndex = 0;
//        }else if (_imageArrayIndex == 0) {
//            firstIndex = _imageArray.count - 1;
//            threeIndex = _imageArrayIndex + 1;
//        }else{
//            firstIndex = _imageArrayIndex - 1;
//            threeIndex = _imageArrayIndex + 1;
//        }
        _imageArrayIndex = (_imageArrayIndex-1 + _imageArray.count)%_imageArray.count;
        
    }
    // == 320 * 2
    else if (self.contentOffset.x > ycScrollViewWidth)
    {
        _imageArrayIndex = (_imageArrayIndex + 1)%_imageArray.count;
//        _imageArrayIndex ++;
//        if (_imageArrayIndex == _imageArray.count-1) {
//            firstIndex = _imageArrayIndex - 1;
//            threeIndex = 0;
//        }
//        else if (_imageArrayIndex > _imageArray.count-1)
//        {
//            _imageArrayIndex = 0;
//            firstIndex = _imageArray.count - 1;
//            threeIndex = _imageArrayIndex + 1;
//        }
//        else
//        {
//            firstIndex = _imageArrayIndex - 1;
//            threeIndex = _imageArrayIndex + 1;
//        }
    }
    else
    {
        return;
    }
    firstIndex = (_imageArrayIndex - 1 + _imageArray.count)%_imageArray.count;
    threeIndex = (_imageArrayIndex + 1)%_imageArray.count;
    
    _pageControl.currentPage = _imageArrayIndex;
    
    
    _firstImageView.image = [UIImage imageNamed:_imageArray[firstIndex]];
    _sencondImageView.image = [UIImage imageNamed:_imageArray[_imageArrayIndex]];
     _threeImageView.image = [UIImage imageNamed:_imageArray[threeIndex]];
    self.contentOffset = CGPointMake(ycScrollViewWidth, 0);
}

//时间方法

- (void)setupTiemr
{
    _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(andTimeAction) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSDefaultRunLoopMode];
}
- (void)andTimeAction{
    [self setContentOffset:CGPointMake(ycScrollViewWidth * 2, 0) animated:YES];
    _isTimerUp = YES;
    [NSTimer scheduledTimerWithTimeInterval:0.4f target:self selector:@selector(scrollViewDidEndDecelerating:) userInfo:nil repeats:NO];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (_timer) {
        [_timer invalidate];
        _timer = nil;
        _isTimerUp = NO;
    }
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!_timer) {
        [self setupTiemr];
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    if (self.block) {
        self.block();
    }
}

- (void)addTouchImageActionBlock:(void (^)(NSInteger))block
{
    __block ycScrollview *scrollview = self;
    self.block = ^(){
        if (block) {
            NSInteger index = scrollview.imageArrayIndex;
            block(index);
        }
    };
}


@end
