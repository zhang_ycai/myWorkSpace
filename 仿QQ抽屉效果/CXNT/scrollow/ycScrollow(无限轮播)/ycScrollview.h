//
//  ycScrollview.h
//  无限轮播图
//
//  Created by cvicseks-mac1 on 16/3/1.
//  Copyright © 2016年 cvicseks-mac1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ycScrollview : UIScrollView

@property (nonatomic, strong) NSMutableArray *imageArray;

- (void)addTouchImageActionBlock:(void (^)(NSInteger index))block; //点击的回调

@end
