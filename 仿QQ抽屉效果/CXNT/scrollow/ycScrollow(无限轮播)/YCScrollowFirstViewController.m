//
//  YCScrollowFirstViewController.m
//  qq
//
//  Created by cvicseks-mac1 on 16/3/1.
//  Copyright © 2016年 zyc. All rights reserved.
//

#import "YCScrollowFirstViewController.h"
#import "ycScrollview.h"
#import "DSToast.h"

@interface YCScrollowFirstViewController ()

@property (nonatomic, strong) ycScrollview *scrollView;

@end

@implementation YCScrollowFirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
//    self.navigationController.navigationBar.translucent = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    _scrollView = [[ycScrollview alloc] initWithFrame:CGRectMake(0, kMaxY, self.view.bounds.size.width, 180)];
    NSArray * array = @[@"h1.jpg",@"h2.jpg",@"h3.jpg",@"h4.jpg"];
    _scrollView.imageArray = [NSMutableArray arrayWithArray:array];
    _scrollView.backgroundColor = [UIColor greenColor];
    __block UIView *view = self.view;
    [_scrollView addTouchImageActionBlock:^(NSInteger index) {
        [[DSToast toastWithText:[NSString stringWithFormat:@"clicked is %ld", (long)index]] showInView:view];
        NSLog(@"%ld", (long)index);
    }];
    [self.view addSubview:_scrollView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
