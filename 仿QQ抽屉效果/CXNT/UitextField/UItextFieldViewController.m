//
//  UItextFieldViewController.m
//  qq
//
//  Created by cvicseks-mac1 on 16/2/25.
//  Copyright © 2016年 zyc. All rights reserved.
//

#import "UItextFieldViewController.h"

@interface UItextFieldViewController ()
<
UITextFieldDelegate
>

@property (nonatomic, strong)UITextField *textFieldFirst;
@property (nonatomic, strong) UIButton *clearBtn;
@property (nonatomic, strong) UIView *inputView;
//button3
@property (nonatomic, strong) UIButton *backBtn;
@end

@implementation UItextFieldViewController

- (UIView *)inputView
{
    if (!_inputView) {
        _inputView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];
        _inputView.backgroundColor = [UIColor lightGrayColor];
    }
    return _inputView;
}

- (UIButton *)clearBtn
{
    if (!_clearBtn) {
        _clearBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 106, 30)];
        [_clearBtn setTitle:@"Clear" forState:UIControlStateNormal];
        [_clearBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    }
    return _clearBtn;
}

- (UIButton *)backBtn
{
    if (!_backBtn) {
        _backBtn = [[UIButton alloc] initWithFrame:self.view.bounds];
        _backBtn.backgroundColor = [UIColor blackColor];
        _backBtn.alpha = 0.5;
        [_backBtn addTarget:self action:@selector(clickedBackBtn) forControlEvents:UIControlEventTouchDragInside];
    }
    return _backBtn;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"UItextField";
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    [self.inputView addSubview:self.clearBtn];
    
    self.textFieldFirst = [[UITextField alloc] initWithFrame:CGRectMake(kScreenWidth/2- 50, kMaxY+10, 100, 30)];
    // 密码
    self.textFieldFirst.secureTextEntry = YES;
    // return键的文本
    self.textFieldFirst.returnKeyType = UIReturnKeyDefault;
    // 是否开启拼写检查
    self.textFieldFirst.spellCheckingType = UITextSpellCheckingTypeYes;
    //    self.textFieldFirst.keyboardType = UIKeyboardTypePhonePad;
    // 自动大写
    //    self.textFieldFirst.autocapitalizationType =
    self.textFieldFirst.placeholder = @"提示文字";
    // 边框样式
    self.textFieldFirst.borderStyle = UITextBorderStyleRoundedRect;
    self.textFieldFirst.delegate = self;
    self.textFieldFirst.inputAccessoryView = self.inputView;
    [self.view addSubview:self.textFieldFirst];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyBoardDidShow:) name:UIKeyboardDidShowNotification object:nil];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)clickedBackBtn
{
    [self.backBtn removeFromSuperview];
}

// 把蒙版放在键盘上面
// 拿到的还是原来的window
//- (void)textFieldDidBeginEditing:(UITextField *)textField
//{
//    UIWindow *window = [[[UIApplication sharedApplication] windows] lastObject];
//    [window addSubview:self.backBtn];
//}

- (void)handleKeyBoardDidShow:(NSNotification*)natification
{
    UIWindow *window = [[[UIApplication sharedApplication] windows] lastObject];
    [window addSubview:self.backBtn];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
