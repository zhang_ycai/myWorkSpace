//
//  CAEmitterViewController.m
//  qq
//
//  Created by cvicseks-mac1 on 16/3/4.
//  Copyright © 2016年 zyc. All rights reserved.
//

#import "CAEmitterViewController.h"

@interface CAEmitterViewController ()
@property (strong) CAEmitterLayer *fireEmitter;
@property (strong) CAEmitterLayer *smokeEmitter;

@end

@implementation CAEmitterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    // 雪花效果
    CAEmitterLayer *snowEmitter = [ CAEmitterLayer layer];
    // 发射位置
    snowEmitter.emitterPosition = CGPointMake(120, 20);
    // 发射器尺寸
    snowEmitter.emitterSize = CGSizeMake(kScreenWidth/2, 20);
    // 发射模式
    snowEmitter.emitterMode = kCAEmitterLayerSurface;
    // 发射源的形状
    snowEmitter.emitterShape = kCAEmitterLayerLine;
    
    // 粒子
    CAEmitterCell *snowflake = [CAEmitterCell emitterCell];
    // 粒子的名字
    snowflake.name = @"snow";
    // 粒子参数的速度乘数因子
    snowflake.birthRate = 1.0;
    snowflake.lifetime = 120.0;
    // 粒子速度
    snowflake.velocity = 10.0;
    // 粒子速度范围
    snowflake.velocityRange = 10;
    // y方向加速度
    snowflake.yAcceleration = 2;
    // 周围发射角度
    snowflake.emissionRange = 0.5*M_PI;
    // 子旋转角度范围
    snowflake.spinRange = 0.25*M_PI;
    
    snowflake.contents = (id)[[UIImage imageNamed:@"flake"] CGImage];
    snowflake.color = [[UIColor colorWithRed:0.200 green:0.258 blue:0.543 alpha:1.000] CGColor];
    
    //创建星星形状的粒子
    CAEmitterCell *snowflake1 = [CAEmitterCell emitterCell];
    //粒子的名字
    snowflake1.name = @"snow1";
    //粒子参数的速度乘数因子
    snowflake1.birthRate = 1.0;
    snowflake1.lifetime = 120.0;
    //粒子速度
    snowflake1.velocity =10.0;
    //粒子的速度范围
    snowflake1.velocityRange = 10;
    //粒子y方向的加速度分量
    snowflake1.yAcceleration = 2;
    //周围发射角度
    snowflake1.emissionRange = 0.5 * M_PI;
    //子旋转角度范围
    snowflake1.spinRange = 0.25 * M_PI;
    //粒子的内容和内容的颜色
//    snowflake1.contents = (id)[[UIImage imageNamed:@DazStarOutline] CGImage];
    snowflake1.color = [[UIColor colorWithRed:0.600 green:0.658 blue:0.743 alpha:1.000] CGColor];
    
    snowEmitter.shadowOpacity = 1.0;
    snowEmitter.shadowRadius = 0.0;
    snowEmitter.shadowOffset = CGSizeMake(0.0, 1.0);
    //粒子边缘的颜色
    snowEmitter.shadowColor = [[UIColor redColor] CGColor];
    
    
    
    
    // Create the emitter layers
    self.fireEmitter	= [CAEmitterLayer layer];
    self.smokeEmitter	= [CAEmitterLayer layer];
    
    // Place layers just above the tab bar
    self.fireEmitter.emitterPosition = CGPointMake(kScreenWidth/2.0, kScreenHeight - 60);
    self.fireEmitter.emitterSize	= CGSizeMake(kScreenWidth/2.0, 0);
    self.fireEmitter.emitterMode	= kCAEmitterLayerOutline;
    self.fireEmitter.emitterShape	= kCAEmitterLayerLine;
    // with additive rendering the dense cell distribution will create "hot" areas
    self.fireEmitter.renderMode		= kCAEmitterLayerAdditive;
    
    self.smokeEmitter.emitterPosition = CGPointMake(kScreenWidth/2.0, kScreenHeight - 60);
    self.smokeEmitter.emitterMode	= kCAEmitterLayerPoints;
    
    // Create the fire emitter cell
    CAEmitterCell* fire = [CAEmitterCell emitterCell];
    [fire setName:@"fire"];
    
    fire.birthRate			= 100;
    fire.emissionLongitude  = M_PI;
    fire.velocity			= -80;
    fire.velocityRange		= 30;
    fire.emissionRange		= 1.1;
    fire.yAcceleration		= -200;
    fire.scaleSpeed			= 0.3;
    fire.lifetime			= 50;
    fire.lifetimeRange		= (50.0 * 0.35);
    
    fire.color = [[UIColor colorWithRed:0.8 green:0.4 blue:0.2 alpha:0.1] CGColor];
    fire.contents = (id) [[UIImage imageNamed:@"DazFire"] CGImage];
    
    
    // Create the smoke emitter cell
    CAEmitterCell* smoke = [CAEmitterCell emitterCell];
    [smoke setName:@"smoke"];
    
    smoke.birthRate			= 11;
    smoke.emissionLongitude = -M_PI / 2;
    smoke.lifetime			= 10;
    smoke.velocity			= -40;
    smoke.velocityRange		= 20;
    smoke.emissionRange		= M_PI / 4;
    smoke.spin				= 1;
    smoke.spinRange			= 6;
    smoke.yAcceleration		= -160;
    smoke.contents			= (id) [[UIImage imageNamed:@"DazSmoke"] CGImage];
    smoke.scale				= 0.1;
    smoke.alphaSpeed		= -0.12;
    smoke.scaleSpeed		= 0.7;
    
    
    // Add the smoke emitter cell to the smoke emitter layer
    self.smokeEmitter.emitterCells	= [NSArray arrayWithObject:smoke];
    self.fireEmitter.emitterCells	= [NSArray arrayWithObject:fire];
    [self.view.layer addSublayer:self.smokeEmitter];
    [self.view.layer addSublayer:self.fireEmitter];
    
    [self setFireAmount:0.9];
    
    snowEmitter.emitterCells = [NSArray arrayWithObjects:snowflake, nil]
    ;
    [self.view.layer insertSublayer:snowEmitter atIndex:0];

}

- (void) setFireAmount:(float)zeroToOne
{
    // Update the fire properties
    [self.fireEmitter setValue:[NSNumber numberWithInt:(zeroToOne * 500)]
                    forKeyPath:@"emitterCells.fire.birthRate"];
    [self.fireEmitter setValue:[NSNumber numberWithFloat:zeroToOne]
                    forKeyPath:@"emitterCells.fire.lifetime"];
    [self.fireEmitter setValue:[NSNumber numberWithFloat:(zeroToOne * 0.35)]
                    forKeyPath:@"emitterCells.fire.lifetimeRange"];
    self.fireEmitter.emitterSize = CGSizeMake(50 * zeroToOne, 0);
    
    [self.smokeEmitter setValue:[NSNumber numberWithInt:zeroToOne * 4]
                     forKeyPath:@"emitterCells.smoke.lifetime"];
    [self.smokeEmitter setValue:(id)[[UIColor colorWithRed:1 green:1 blue:1 alpha:zeroToOne * 0.3] CGColor]
                     forKeyPath:@"emitterCells.smoke.color"];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 代码设置/结构说明：
 
 1、CAEmitterCell
 
 CAEmitterCell ＊effectCell = [CAEmitterCell emitterCell];
 
 effectCell 几个重要属性：
 
 1）.birthRate 顾名思义没有这个也就没有effectCell，这个必须要设置，具体含义是每秒某个点产生的effectCell数量
 
 2）.lifetime & lifetimeRange 表示effectCell的生命周期，既在屏幕上的显示时间要多长。
 
 3）.contents 这个和CALayer一样，只是用来设置图片
 
 4）.name 这个是当effectCell存在caeEmitter 的emitterCells中用来辨认的。用到setValue forKeyPath比较有用
 
 5）.velocity & velocityRange & emissionRange 表示cell向屏幕右边飞行的速度 & 在右边什么范围内飞行& ＋－角度扩散
 
 6）.把cell做成array放进caeEmitter.emitterCells里去。caeEmitter.renderMode有个效果很不错，能变成火的就是kCAEmitterLayerAdditive
 
 属性：
 
 alphaRange:  一个粒子的颜色alpha能改变的范围；
 
 alphaSpeed:粒子透明度在生命周期内的改变速度；
 
 birthrate：粒子参数的速度乘数因子；
 
 blueRange：一个粒子的颜色blue 能改变的范围；
 
 blueSpeed: 粒子blue在生命周期内的改变速度；
 
 color:粒子的颜色
 
 contents：是个CGImageRef的对象,既粒子要展现的图片；
 
 contentsRect：应该画在contents里的子rectangle：
 
 emissionLatitude：发射的z轴方向的角度
 
 emissionLongitude:x-y平面的发射方向
 
 emissionRange；周围发射角度
 
 emitterCells：粒子发射的粒子
 
 enabled：粒子是否被渲染
 
 greenrange: 一个粒子的颜色green 能改变的范围；
 
 greenSpeed: 粒子green在生命周期内的改变速度；
 
 lifetime：生命周期
 
 lifetimeRange：生命周期范围
 
 magnificationFilter：不是很清楚好像增加自己的大小
 
 minificatonFilter：减小自己的大小
 
 minificationFilterBias：减小大小的因子
 
 name：粒子的名字
 
 redRange：一个粒子的颜色red 能改变的范围；
 
 redSpeed; 粒子red在生命周期内的改变速度；
 
 scale：缩放比例：
 
 scaleRange：缩放比例范围；
 
 scaleSpeed：缩放比例速度：
 
 spin：子旋转角度
 
 spinrange：子旋转角度范围
 
 style：不是很清楚：
 
 velocity：速度
 
 velocityRange：速度范围
 
 xAcceleration:粒子x方向的加速度分量
 
 yAcceleration:粒子y方向的加速度分量
 
 zAcceleration:粒子z方向的加速度分量
 
 
 2、CAEmitterLayer
 CAEmitterLayer提供了一个基于Core Animation的粒子发射系统，粒子用CAEmitterCell来初始化。粒子画在背景层盒边界上
 
 属性:
 
 birthRate:粒子产生系数，默认1.0；
 
 emitterCells: 装着CAEmitterCell对象的数组，被用于把粒子投放到layer上；
 
 emitterDepth:决定粒子形状的深度联系：emittershape
 
 emitterMode:发射模式
 
 NSString * const kCAEmitterLayerPoints;
 
 NSString * const kCAEmitterLayerOutline;
 
 NSString * const kCAEmitterLayerSurface;
 
 NSString * const kCAEmitterLayerVolume;
 
 
 
 emitterPosition:发射位置
 
 emitterShape:发射源的形状：
 
 NSString * const kCAEmitterLayerPoint;
 
 NSString * const kCAEmitterLayerLine;
 
 NSString * const kCAEmitterLayerRectangle;
 
 NSString * const kCAEmitterLayerCuboid;
 
 NSString * const kCAEmitterLayerCircle;
 
 NSString * const kCAEmitterLayerSphere;
 
 
 
 emitterSize:发射源的尺寸大；
 
 emitterZposition:发射源的z坐标位置；
 
 lifetime:粒子生命周期
 
 preservesDepth:不是多很清楚（粒子是平展在层上）
 
 renderMode:渲染模式：
 
 NSString * const kCAEmitterLayerUnordered;
 
 NSString * const kCAEmitterLayerOldestFirst;
 
 NSString * const kCAEmitterLayerOldestLast;
 
 NSString * const kCAEmitterLayerBackToFront;
 
 NSString * const kCAEmitterLayerAdditive;
 
 
 
 scale:粒子的缩放比例：
 
 seed：用于初始化随机数产生的种子
 
 spin:自旋转速度
 
 velocity：粒子速度
 
 Class Methods
 
 defauleValueForKey: 更具健 获 得 值 ；
 
 emitterCell ：初始化方法
 
 shouldArchiveValueForKey: 是否 归 档莫 键值
*/

@end
