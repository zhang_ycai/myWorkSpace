//
//  ShiDiQuanViewController.m
//  qq
//
//  Created by cvicseks-mac1 on 16/3/7.
//  Copyright © 2016年 zyc. All rights reserved.
//

#import "ShiDiQuanViewController.h"
#import "AFNetManager.h"
#import "AFNetworking.h"

@interface ShiDiQuanViewController ()
<
    UICollectionViewDataSource,
    UICollectionViewDelegate
>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *Data;
@property (nonatomic, strong) void (^block)();
@property (nonatomic, strong) void (^failBlock)();

typedef void (^succeedBlock)(NSDictionary *dict);
typedef void (^failBlock)(NSError *error);
@end

@implementation ShiDiQuanViewController

- (UICollectionView *)collectionView
{
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake(kScreenWidth/2, kScreenWidth/2);
        layout.sectionInset = UIEdgeInsetsMake(1, 1, 1, 1);
        layout.minimumInteritemSpacing = 0;
        layout.minimumLineSpacing = 0;
        // 弊端 当header view的高度不同时  貌似不能修改
        layout.headerReferenceSize = CGSizeMake(kScreenWidth, 80);
        _collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
    }
    return _collectionView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:_collectionView];
    
//    [self getData];
    [self netDataRequestWithUrl:@"http://www.shidiquan.cn/shidiquan/app/appservice" parameters:@{@"code":@"getProductList",@"verson":@"1.0",
                                                                                                 @"data":@{@"productType":@"",@"searchWord":@"",@"locX":@31,@"locY":@180,@"currentPage":@1,@"showCount":@1}} types:@"POST" succeedBlock:^(NSDictionary *dict) {
                                                                                                     NSLog(@"----%@",dict);
                                                                                                 } failBlock:^(NSError *error) {
                                                                                                     NSLog(@"%---@", error);
                                                                                                 }];
}

- (void)getData
{
//    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
//    [parameters setObject:@"getProductList" forKey:@"code"];
//    [parameters setObject:@"locX" forKey:@"100"];
//    [parameters setObject:@"locY" forKey:@"100"];
//    [AFNetManager postRequestWithURL:@"http://www.shidiquan.cn/shidiquan/app/appservice" withParameters:@{@"locX":@100,@"locY":@200} success:^(id response) {
//        NSString *request = response;
//    }];
    //1.
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [manager.requestSerializer setStringEncoding:NSUTF8StringEncoding];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    //2.发起post请求
    [manager POST:@"http://www.shidiquan.cn/shidiquan/app/appservice"
       parameters:@{@"code":@"getProductList",@"verson":@"1.0",
                    @"data":@{@"productType":@"",@"searchWord":@"",@"locX":@31,@"locY":@180,@"currentPage":@1,@"showCount":@1}}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              //请求成功
              /*
               id responseObject --> 返回的数据
               */
              NSLog(@"POST 请求成功!");
              NSLog(@"---------------%@",responseObject);
              NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil] ;         }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              //请求失败
              NSLog(@"POST 失败,错误信息: %@",error);
          }];

}

- (void)addMJRefresh
{}

- (void)netDataRequestWithUrl:(NSString *)urlString parameters:(NSDictionary *)parameters types:(NSString *)typeString succeedBlock:(succeedBlock)succeedBlock failBlock:(failBlock)failBlock
{
    AFHTTPRequestOperationManager *request = [AFHTTPRequestOperationManager manager];
    request.requestSerializer = [AFHTTPRequestSerializer serializer];
    [request.requestSerializer setStringEncoding:NSUTF8StringEncoding];
    request.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    request.requestSerializer = [AFJSONRequestSerializer serializer];
    request.responseSerializer = [AFJSONResponseSerializer serializer];
    //超时时间为 30s
    request.requestSerializer.timeoutInterval = 30.0f;
    
    if ([typeString isEqualToString:@"POST"]) {
        [request POST:urlString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            //            NSLog(@"%@",responseObject);
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
            if (dict) {
                succeedBlock(dict);
            } else {
                NSLog(@"返回的数据错误！");
                failBlock(nil);
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"网络加载失败－%@",error);
            failBlock(error);
        }];
    } else if ([typeString isEqualToString:@"GET"]) {
        [request GET:urlString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            //            NSLog(@"%@",responseObject);
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
            if (dict) {
                succeedBlock(dict);
            } else {
                NSLog(@"返回的数据错误！");
                failBlock(nil);
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"网络加载失败－%@",error);
            failBlock(error);
        }];
    }
}
#pragma mark -- collection Delegate
// 一个区有几个 item 注:在collection view 中 每个cell不叫cell 叫item
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 3;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString * const identifierID = @"collectionCell";
    UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifierID forIndexPath:indexPath];
    cell.backgroundColor = [UIColor colorWithRed:(arc4random()%255)/255.0 green:(arc4random()%255)/255.0 blue:(arc4random()%255)/255.0 alpha:1];
    
    return cell;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
