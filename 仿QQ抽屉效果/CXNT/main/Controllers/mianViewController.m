//
//  mianViewController.m
//  qq
//
//  Created by cvicseks-mac1 on 16/2/22.
//  Copyright © 2016年 zyc. All rights reserved.
//

#import "mianViewController.h"
#import "XWDragCellCollectionView.h"
#import "mainCollectionViewCell.h"
#import "DocumentManager.h"
#import "collectionCellModel.h"

@interface mianViewController ()
<
    XWDragCellCollectionViewDataSource,
    XWDragCellCollectionViewDelegate
>

@property (nonatomic, strong) XWDragCellCollectionView *collectionView;
@property (nonatomic, strong) NSArray *data;

@end

@implementation mianViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBar.tintColor = [UIColor redColor];
    // 改变默认按钮颜色
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(50, 50, 60, 60)];
    [button setImage:[UIImage imageNamed:@"home_1_bus@2x.png"] forState:UIControlStateNormal];
    [self.view addSubview:button];
    [self createCollectionView];
    // Do any additional setup after loading the view.
    //去除导航栏底部标识线
//    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc]init] forBarMetrics:UIBarMetricsDefault];
//    self.navigationController.navigationBar.shadowImage = [[UIImage alloc]init];
    //修改导航栏偏色
//    self.navigationController.navigationBar.translucent = NO;
}

- (XWDragCellCollectionView *)collectionView
{
    if (!_collectionView) {
        
    }
    
    return _collectionView;
}

- (void)createCollectionView
{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(80, 80);
    // 与边框的位置关系
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
//    layout.estimatedItemSize = CGSizeMake(0, 0);
    layout.minimumInteritemSpacing = 0;// 横间距
    layout.minimumLineSpacing = 0;// 竖间距
    // item布局方式
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    _collectionView = [[XWDragCellCollectionView alloc] initWithFrame:CGRectMake(0, 200, kScreenWidth, kScreenWidth) collectionViewLayout:layout];
    DLog(@"%f",kScreenWidth);
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.backgroundColor = [UIColor whiteColor];
    [_collectionView registerNib:[UINib nibWithNibName:@"mainCollectionViewCell" bundle:nil ] forCellWithReuseIdentifier:@"mainCollectionViewCell"];
    [self.view addSubview:_collectionView];
}
- (NSArray *)data
{
    if (!_data) {
        DocumentManager *manager = [[DocumentManager alloc] init];
//        [manager createDocumentFileName:@"collectionItem" type:@"plist"];
        [manager createDocumentFileName:@"MenuItem" type:@"plist"];
//        NSMutableDictionary *dataDic = [[NSMutableDictionary alloc] initWithContentsOfFile:[manager getDocumentPath:@"collectionItem.plist"]];
        NSMutableArray *dataDic = [[NSMutableArray alloc] initWithContentsOfFile:[manager getDocumentPath:@"MenuItem.plist"]];
        
        _data = [[NSArray alloc] initWithArray:dataDic];
    }
    return _data;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.data.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"mainCollectionViewCell";
    mainCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    if (!cell) {
        cell = [[mainCollectionViewCell alloc] init];
    }
    collectionCellModel *model = [[collectionCellModel alloc] init];
//    NSArray *array = self.data[[indexPath row]];
//    model.text = array[0];
//    model.imageName = array[1];
    cell.cellModel = [model initWithDic:self.data[indexPath.row]];
//    cell.layer.borderWidth = 1;
//    cell.layer.borderColor = [UIColor redColor].CGColor;
    return cell;
}

- (NSArray *)dataSourceArrayOfCollectionView:(XWDragCellCollectionView *)collectionView
{
    return _data;
}

- (void)dragCellCollectionView:(XWDragCellCollectionView *)collectionView newDataArrayAfterMove:(NSArray *)newDataArray
{
    _data = newDataArray;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
