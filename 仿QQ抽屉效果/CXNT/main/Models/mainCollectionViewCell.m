//
//  mainCollectionViewCell.m
//  qq
//
//  Created by cvicseks-mac1 on 16/2/22.
//  Copyright © 2016年 zyc. All rights reserved.
//

#import "mainCollectionViewCell.h"
#import "collectionCellModel.h"

@implementation mainCollectionViewCell

- (void)awakeFromNib {
    
}

- (void)setCellModel:(collectionCellModel *)cellModel
{
    _cellModel = cellModel;
    [self.imageBtn setImage:[UIImage imageNamed:cellModel.imageName] forState:UIControlStateNormal];
    self.btnText.text = cellModel.text;
    self.deleteBtn.hidden = cellModel.enable;
}

- (IBAction)imageBtnAction:(id)sender {
    NSLog(@"clecked me");
}
@end
