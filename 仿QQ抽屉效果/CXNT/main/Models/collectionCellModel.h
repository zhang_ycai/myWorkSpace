//
//  collectionCellModel.h
//  qq
//
//  Created by cvicseks-mac1 on 16/2/24.
//  Copyright © 2016年 zyc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface collectionCellModel : NSObject

@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *imageName;
@property (nonatomic, strong) NSString *ViewController;
@property (nonatomic, assign) BOOL enable;

-(instancetype)initWithDic:(NSDictionary *)Dict;
@end
