//
//  collectionCellModel.m
//  qq
//
//  Created by cvicseks-mac1 on 16/2/24.
//  Copyright © 2016年 zyc. All rights reserved.
//

#import "collectionCellModel.h"

@implementation collectionCellModel

- (instancetype)initWithDic:(NSDictionary *)Dict
{
    if (self = [super init]) {
        self.text = [Dict objectForKey:@"Text"];
        self.imageName = [Dict objectForKey:@"ImageName"];
        self.ViewController = [Dict objectForKey:@"ViewController"];
    }
    return self;
}

@end
