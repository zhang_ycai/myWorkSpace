//
//  mainCollectionViewCell.h
//  qq
//
//  Created by cvicseks-mac1 on 16/2/22.
//  Copyright © 2016年 zyc. All rights reserved.
//

#import <UIKit/UIKit.h>
@class collectionCellModel;
@interface mainCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIButton *imageBtn;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UILabel *btnText;
@property (strong, nonatomic) collectionCellModel *cellModel;
- (IBAction)imageBtnAction:(id)sender;

@end
