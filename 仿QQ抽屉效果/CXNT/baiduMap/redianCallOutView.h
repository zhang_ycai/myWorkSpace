//
//  redianCallOutView.h
//  qq
//
//  Created by cvicseks-mac1 on 16/1/20.
//  Copyright © 2016年 zyc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@protocol walkCustomCalloutViewDelegate <NSObject>

@optional
- (void)walkGetRoute:(CLLocationCoordinate2D)coordinate;

@end
@interface redianCallOutView : UIView

@property (nonatomic, strong) UILabel *nameLabel; // 当前位置信息
@property (nonatomic, strong) UILabel *distance; // 与当前位置距离
@property (nonatomic, strong) UIButton *routeBtn; // 路线按钮
@property (nonatomic, assign) int itemNum; // 热点计数
@property (nonatomic, assign) CLLocationCoordinate2D coordinate; // 当前位置的经纬度
@property (nonatomic, strong) UIImageView *backGround; // 气泡

@property (nonatomic, weak) id<walkCustomCalloutViewDelegate>delegate;
@end

