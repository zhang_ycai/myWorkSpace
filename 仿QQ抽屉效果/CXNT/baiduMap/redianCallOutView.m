//
//  redianCallOutView.m
//  qq
//
//  Created by cvicseks-mac1 on 16/1/20.
//  Copyright © 2016年 zyc. All rights reserved.
//

#import "redianCallOutView.h"

@implementation redianCallOutView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        self.backGround = [[UIImageView alloc] initWithFrame:frame];
        UIImage *image          = [UIImage imageNamed:@"redianCallOutViewBackground"];
        self.backGround.image = image;
        [self addSubview:self.backGround];
        
        /* Create name label. */
        self.nameLabel =[[UILabel alloc] initWithFrame:CGRectMake(5, 5, self.frame.size.width-50-5, 35)];
        self.nameLabel.backgroundColor  = [UIColor clearColor];
        self.nameLabel.textAlignment    = NSTextAlignmentCenter;
        self.nameLabel.textColor        = [UIColor colorWithRed:54/255.0 green:54/255.0 blue:54/255.0 alpha:1.0];
        self.nameLabel.font             = [UIFont systemFontOfSize:13.f];
        self.nameLabel.lineBreakMode    = UILineBreakModeWordWrap;
        self.nameLabel.numberOfLines    = 0;
        [self addSubview:self.nameLabel];
        
        
        /* Create distance. */
        self.distance                 =[[UILabel alloc] initWithFrame:CGRectMake(10, 46, self.frame.size.width-60-10, 25)];
        self.distance.backgroundColor =[UIColor clearColor];
        self.distance.textAlignment   =NSTextAlignmentCenter;
        self.distance.textColor       =[UIColor colorWithRed:102/255.0 green:102/255.0 blue:102/255.0 alpha:1.0];
        self.distance.font            =[UIFont systemFontOfSize:13];
        [self addSubview:self.distance];
        // 收藏按钮
        /* Create routeBtn. */
        self.routeBtn                =[[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width-45, 0, 45, 80)];
        [self.routeBtn.layer setMasksToBounds:YES];
        [self.routeBtn.layer setCornerRadius:10.0];//设置矩形四个圆角半径
        self.routeBtn.backgroundColor =[UIColor clearColor];
        UIImageView *imageview        = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"路线_步行"]];
        imageview.frame               = CGRectMake(10, 15, 30, 30);
        imageview.backgroundColor     = [UIColor clearColor];
        [self.routeBtn addSubview:imageview];
        UILabel *label                = [[UILabel alloc] initWithFrame:CGRectMake(5, 46, 40, 20)];
        label.text = @"路线";
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont systemFontOfSize:11];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor whiteColor];
        [self.routeBtn addSubview:label];
        [self.routeBtn addTarget:self action:@selector(clicked) forControlEvents:UIControlEventTouchUpInside];
        
        
        [self addSubview:self.routeBtn];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
    
}

- (void)clicked
{
    [self.delegate walkGetRoute:self.coordinate];
}


@end
