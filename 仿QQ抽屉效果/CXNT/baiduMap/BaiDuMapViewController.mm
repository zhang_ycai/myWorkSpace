//
//  BaiDuMapViewController.m
//  qq
//
//  Created by cvicseks-mac1 on 16/1/19.
//  Copyright © 2016年 zyc. All rights reserved.
//

#import "BaiDuMapViewController.h"
#import <BaiduMapAPI_Map/BMKMapComponent.h>           // 引入地图功能所有的头文件
#import <BaiduMapAPI_Location/BMKLocationComponent.h> // 引入定位功能所有的头文件
#import <BaiduMapAPI_Search/BMKSearchComponent.h>     // 引入检索功能所有的头文件
#import <BaiduMapAPI_Utils/BMKUtilsComponent.h>       // 引入计算工具所有的头文件
#import "CheckGPSLocation.h"
#import "CheckNetWork.h"
#import "redianCallOutView.h"
#import "UIImage+Rotate.h"

#define MYBUNDLE_NAME @ "mapapi.bundle"
#define MYBUNDLE_PATH [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent: MYBUNDLE_NAME]
#define MYBUNDLE [NSBundle bundleWithPath: MYBUNDLE_PATH]

// 热点标注 不能有重名的
@interface BMKReDianAnnotationView : BMKPinAnnotationView
{
    int _num;
    CLLocationCoordinate2D coordinate;
}
@property (nonatomic) int num;
@property (nonatomic) CLLocationCoordinate2D coordinate;
@end

@implementation BMKReDianAnnotationView
@synthesize num = _num;
@synthesize coordinate = _coordinate;
@end

@interface RouteAnnotation : BMKPointAnnotation
{
    int _type; ///<0:起点 1：终点 2：公交 3：地铁 4:驾乘 5:途经点
    int _degree;
}

@property (nonatomic) int type;
@property (nonatomic) int degree;
@end

@implementation RouteAnnotation

@synthesize type = _type;
@synthesize degree = _degree;
@end


@interface BaiDuMapViewController ()
<
    BMKMapViewDelegate ,// 地图view代理
    BMKLocationServiceDelegate,
    BMKSuggestionSearchDelegate,
    BMKPoiSearchDelegate,
    walkCustomCalloutViewDelegate,
    BMKRouteSearchDelegate,
    UIAlertViewDelegate,
    UITextFieldDelegate,
    UITableViewDataSource,
    UITableViewDelegate
>
{
    BMKLocationService *locationService;
    BMKPoiSearch       *poiSearch;
    BMKPointAnnotation *redianAnnotation; // 热点的点
    BMKUserLocation    *baiduUserLocation;
    BMKRouteSearch     *routeSearch;
    BMKSuggestionSearch *suggestionSearch;
    UIButton           *searchBtn;
    UIView             *speechSearchView;
    UITextField        *destinationField;
    UITableView        *poiTabelView;
    UITableView        *moreFunctionTableView;
    NSMutableArray     *poiResultArray;
    NSMutableArray     *suggestionArray;
    NSArray            *moreFunctionArray;
    NSArray            *gestureRecognizersarrge;
    int                 redianNum;
    
}

@property (nonatomic, strong) BMKMapView *mapView;
@property (nonatomic, strong) UIButton *userLocationBtn;

@end

@implementation BaiDuMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self removeGestureRecognizer];
    
    [self initNavigation];
    
    [self initMapView];
    
    [self initUserLocationBtn];
    
    [self createSearchBtn];
    
    locationService = [[BMKLocationService alloc] init];
    locationService.delegate = self;
    // 必须在info.plist文件中添加以下选项，否则无法获取GPS服务
    //NSLocationWhenInUseUsageDescription ，允许在前台使用时获取GPS的描述
    //NSLocationAlwaysUsageDescription ，允许永久使用GPS的描述
    
    [CheckGPSLocation locationServicesEnabledanddelegate:self];
    
    poiSearch = [[BMKPoiSearch alloc] init];
    routeSearch = [[BMKRouteSearch alloc] init];
    suggestionSearch = [[BMKSuggestionSearch alloc] init];
    
    [self initMoreFunctionArray];
    
    poiResultArray = [[NSMutableArray alloc] init];
    suggestionArray = [[NSMutableArray alloc] init];
    
    redianNum = 1;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [_mapView viewWillAppear];
    _mapView.delegate = self; // 此处记得不用的时候需要置nil，否则影响内存的释放
    locationService.delegate = self;
    poiSearch.delegate = self;
    routeSearch.delegate = self;
    suggestionSearch.delegate = self;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_mapView viewWillDisappear];
    _mapView.delegate = nil; // 不用时，置nil
    locationService.delegate = nil;
    poiSearch.delegate = nil;
    routeSearch.delegate = nil;
    suggestionSearch.delegate = nil;
}

#pragma mark - 初始化

- (void)initNavigation
{
    self.navigationItem.title = @"百度地图";
    UIButton *rightBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [rightBtn setImage:[UIImage imageNamed:@"moreFunction"] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(moreFunction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBtnItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightBtnItem;
    
    UIBarButtonItem *leftBtnItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(back)];
    self.navigationItem.leftBarButtonItem = leftBtnItem;
}

- (void)initMapView
{
    // 百度地图初始化一定要和frame放在一起
    _mapView  = [[BMKMapView alloc] init];
//    _mapView.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
    _mapView.frame = self.view.frame;
    _mapView.mapType = BMKMapTypeStandard;
    _mapView.showsUserLocation = YES;
    _mapView.isSelectedAnnotationViewFront = YES;
    [_mapView setZoomEnabled:YES];
    [_mapView setZoomLevel:15];
    [self.view addSubview:_mapView];
}

- (void)initUserLocationBtn
{
    _userLocationBtn =[[UIButton alloc] initWithFrame:CGRectMake(5, _mapView.frame.size.height-120, 50, 50)];
    [_userLocationBtn setImage:[UIImage imageNamed:@"userLocationBtn.png"] forState:UIControlStateNormal];
    _userLocationBtn.backgroundColor =[UIColor clearColor];
    [_userLocationBtn addTarget:self action:@selector(getUserLocation) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_userLocationBtn];
}

#pragma mark - 返回

- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
    [self addGestureRecognizer];
}

#pragma mark - 移除手势

- (void)removeGestureRecognizer
{
    //移除手势
    UINavigationController *navigation = self.navigationController;
    gestureRecognizersarrge = navigation.view.gestureRecognizers;
    for (int i = 0; i < gestureRecognizersarrge.count; i++) {
        NSString * gesName = [NSString stringWithUTF8String:object_getClassName([gestureRecognizersarrge objectAtIndex:i])];
        if ([gesName isEqualToString:@"UIPanGestureRecognizer"]) {
            [self.navigationController.view removeGestureRecognizer:[gestureRecognizersarrge objectAtIndex:i]];
        }
        if ([gesName isEqualToString:@"UISwipeGestureRecognizer"]) {
            [self.navigationController.view removeGestureRecognizer:[gestureRecognizersarrge objectAtIndex:i]];
        }
    }

}

- (void)addGestureRecognizer
{
    //添加手势
    for (int i = 0; i < gestureRecognizersarrge.count; i++) {
        NSString * gesName = [NSString stringWithUTF8String:object_getClassName([gestureRecognizersarrge objectAtIndex:i])];
        if ([gesName isEqualToString:@"UIPanGestureRecognizer"]) {
            [self.navigationController.view addGestureRecognizer:[gestureRecognizersarrge objectAtIndex:i]];
        }
        if ([gesName isEqualToString:@"UISwipeGestureRecognizer"]) {
            [self.navigationController.view addGestureRecognizer:[gestureRecognizersarrge objectAtIndex:i]];
        }
    }

}

#pragma mark - moreFunction

- (void)moreFunction
{
    [self initMoreFunctionTableView];
}

- (void)initMoreFunctionTableView
{
    if (!moreFunctionTableView) {
        moreFunctionTableView = [[UITableView alloc] initWithFrame:CGRectMake(kScreenWidth-130-2, kMaxY+2, 130, 40*moreFunctionArray.count)];
        moreFunctionTableView.rowHeight = 40;
        moreFunctionTableView.delegate = self;
        moreFunctionTableView.dataSource = self;
        moreFunctionTableView.tag = 102;
        moreFunctionTableView.bounces = NO;
        moreFunctionTableView.showsVerticalScrollIndicator = NO;
        [self.view addSubview:moreFunctionTableView];
        [moreFunctionTableView reloadData];
    }else
    {
        [moreFunctionTableView removeFromSuperview];
        moreFunctionTableView = nil;
    }
}

- (void)initMoreFunctionArray
{
    if (!moreFunctionArray) {
        moreFunctionArray = [[NSArray alloc] initWithObjects:@"poiSearch",@"suggestionSearch", nil];
    }
}

#pragma mark - 懒加载
- (BMKMapView *)mapView
{
    if (_mapView == nil) {
//        _mapView = [[BMKMapView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
    }
    return _mapView;
}
#pragma mark - BMKMapViewDelegate-基本代理方法

- (void)mapViewDidFinishLoading:(BMKMapView *)mapView
{
    DLog(@"mapViewDidFinishLoading");
    // 定位一定要先开启服务
    [locationService startUserLocationService];
    [self startFollowing];
}

- (void)mapView:(BMKMapView *)mapView onClickedMapBlank:(CLLocationCoordinate2D)coordinate
{
    DLog(@"map view: click blank");
}
- (void)mapview:(BMKMapView *)mapView onDoubleClick:(CLLocationCoordinate2D)coordinate {
    DLog(@"map view: double click");
}

- (BMKAnnotationView *)mapView:(BMKMapView *)mapView viewForAnnotation:(id<BMKAnnotation>)annotation
{
    // 跟换定位图标 通过BMKLocationViewDisplayParam来更改
    
    // 热点图标
    if (annotation == redianAnnotation) {
        NSString *AnnotationViewID = @"redianAnnotation";
        // 热点对应的标注
        BMKReDianAnnotationView *redianAnnotationView = (BMKReDianAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationViewID];
        if (redianAnnotationView == nil) {
            redianAnnotationView = [[BMKReDianAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationViewID];
        }
        // 设置颜色
        redianAnnotationView.pinColor = BMKPinAnnotationColorPurple;
        // 设置能够弹出气泡
        redianAnnotationView.canShowCallout = YES;
        // 从天上掉下效果
        redianAnnotationView.animatesDrop = NO;
        // 设置可拖拽
        redianAnnotationView.draggable = NO;
        redianAnnotationView.image = [UIImage imageNamed:[NSString stringWithFormat:@"热点_%i红",redianNum]];
        redianAnnotationView.calloutOffset = CGPointMake(0, -5);
        redianAnnotationView.num = redianNum;
        redianAnnotationView.coordinate = redianAnnotation.coordinate;
        // 设置第一个热点默认点击
        if (redianNum == 1) {
            redianAnnotationView.selected = YES;
            redianAnnotationView.image = [UIImage imageNamed:[NSString stringWithFormat:@"热点_%i蓝",redianNum]];
            _mapView.centerCoordinate = redianAnnotation.coordinate;
        }
        redianCallOutView *paopaoView = [[redianCallOutView alloc] initWithFrame:CGRectMake(0, 0, 160, 80)];
        //            paopaoView.backGround.image = [UIImage imageNamed:@"redianCallOutViewBackground"]; // 可以更换背景
        paopaoView.delegate = self;
        paopaoView.nameLabel.text = redianAnnotation.title;
        paopaoView.coordinate = redianAnnotation.coordinate;
        paopaoView.itemNum = redianNum;
        paopaoView.distance.text = [self distanceBetweenmyLocationAndBikePointLat:redianAnnotation.coordinate];
        redianAnnotationView.paopaoView = [[BMKActionPaopaoView alloc] initWithCustomView:paopaoView];
        if (redianNum == poiResultArray.count) {
            redianNum = 0;
        }
        redianNum++;
        return redianAnnotationView;
    }
    
    if ([annotation isKindOfClass:[RouteAnnotation class]]) {
        return [self getRouteAnnotationView:mapView viewForAnnotation:(RouteAnnotation*)annotation];
    }

    
    return nil;
}

// 百度地图标注点击方法
- (void)mapView:(BMKMapView *)mapView didSelectAnnotationView:(BMKAnnotationView *)view
{
    if ([view isKindOfClass:[BMKReDianAnnotationView class]] ) {
        BMKReDianAnnotationView *selectView = (BMKReDianAnnotationView *)view;
        if (selectView.selected) {
            selectView.image = [UIImage imageNamed:[NSString stringWithFormat:@"热点_%i蓝",selectView.num]];
            _mapView.centerCoordinate = selectView.coordinate;
        }
    }
}
/**
 *当*取消*选中一个annotation views时，调用此接口
 *@param mapView 地图View
 *@param views 取消选中的annotation views
 */
- (void)mapView:(BMKMapView *)mapView didDeselectAnnotationView:(BMKAnnotationView *)view
{
    if ([view isKindOfClass:[BMKReDianAnnotationView class]] ) {
        BMKReDianAnnotationView *selectView = (BMKReDianAnnotationView *)view;
        if (!selectView.selected) {
            selectView.image = [UIImage imageNamed:[NSString stringWithFormat:@"热点_%i红",selectView.num]];
        }
    }
}


#pragma mark - 计算两点之间的距离
// 计算距离

- (NSString *)distanceBetweenmyLocationAndBikePointLat:(CLLocationCoordinate2D) destinationPoint
{
    CLLocationCoordinate2D userLocation = CLLocationCoordinate2DMake (baiduUserLocation.location.coordinate.latitude, baiduUserLocation.location.coordinate.longitude);
    
    BMKMapPoint myPoint = BMKMapPointForCoordinate(userLocation);
    BMKMapPoint bikePoint = BMKMapPointForCoordinate(destinationPoint);
    
    
    CLLocationDistance distance = BMKMetersBetweenMapPoints(myPoint, bikePoint);
    if (distance < 0) {
        distance = 0;
    }
    NSString *dis = [NSString stringWithFormat:@"距离%0.2fKm",distance/1000];
    
    // 角度转换为弧度
    //    double ew1 = self.userLocation.location.coordinate.longitude * DEF_PI180;
    //    double ns1 = self.userLocation.location.coordinate.latitude * DEF_PI180;
    //    double ew2 = [lot doubleValue] * DEF_PI180;
    //    double ns2 = [lat doubleValue] * DEF_PI180;
    //
    //    // 精度差
    //    double dew = ew1 - ew2;
    //
    //    // 若跨东经和西经180度，进行调整
    //    if (dew > DEF_PI)
    //    {
    //        dew = DEF_2PI - dew;
    //    }
    //    else if (dew < -DEF_PI)
    //    {
    //        dew = DEF_2PI + dew;
    //    }
    //
    //    double dx = DEF_R * cos(ns1) * dew;
    //    double dy = DEF_R * (ns1 - ns2);
    //    double distance = sqrt(dx * dx + dy * dy);
    //
    //    NSString *dis = [NSString stringWithFormat:@"%fKm",distance];
    
    return dis;
}


#pragma mark - 定位按钮方法
- (void)getUserLocation
{
    // 定位一定要先开启服务
    [locationService startUserLocationService];
    [self startFollowing];
}
#pragma mark - 三种定位状态
-(void)startLocation
{
    DLog(@"进入普通定位态");
    
    _mapView.showsUserLocation = NO;//先关闭显示的定位图层
    _mapView.userTrackingMode = BMKUserTrackingModeNone;//设置定位的状态
    _mapView.showsUserLocation = YES;//显示定位图层
}
//罗盘态
-(void)startFollowHeading
{
    DLog(@"进入罗盘态");
    
    _mapView.showsUserLocation = NO;
    _mapView.userTrackingMode = BMKUserTrackingModeFollowWithHeading;
    _mapView.showsUserLocation = YES;
    
}
//跟随态 如果要进入地图立即显示设备位置
-(void)startFollowing
{
    DLog(@"进入跟随态");
    
    _mapView.showsUserLocation = NO;
    _mapView.userTrackingMode = BMKUserTrackingModeFollow;
    _mapView.showsUserLocation = YES;
    
}
//停止定位
-(void)stopLocation
{
    DLog(@"停止定位");
    [locationService stopUserLocationService];
    _mapView.showsUserLocation = NO;
}
#pragma mark - BMKLocationdelegate
- (void)willStartLocatingUser
{
    DLog(@"willStartLocatingUser");
}

- (void)didUpdateUserHeading:(BMKUserLocation *)userLocation
{
//    [_mapView updateLocationData:userLocation]; // 方向更新的时候调用
//    _mapView.centerCoordinate = userLocation.location.coordinate;
    DLog(@"didUpdateUserHeading");
}

- (void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation
{
    // 高德地图可以直接从mapview上拿到userlocation
    /**
     *动态更新我的位置数据
     *	@param	[in]	userLocation	定位数据
     */
    [_mapView updateLocationData:userLocation];
    [self isAccuracyCircleShowNO];
    baiduUserLocation = userLocation;
    // 每次定位都把窗口移动到设备位置
//    _mapView.centerCoordinate = userLocation.location.coordinate;
    DLog(@"didUpdateBMKUserLocation");
}

- (void)didStopLocatingUser
{
    DLog(@"didStopLocatingUser");
}
- (void)didFailToLocateUserWithError:(NSError *)error
{
    DLog(@"location error");
}

#pragma mark - routedelegate

- (BMKAnnotationView*)getRouteAnnotationView:(BMKMapView *)mapview viewForAnnotation:(RouteAnnotation*)routeAnnotation
{
    BMKAnnotationView* view = nil;
    switch (routeAnnotation.type) {
        case 0:
        {
            view = [mapview dequeueReusableAnnotationViewWithIdentifier:@"start_node"];
            if (view == nil) {
                view = [[BMKAnnotationView alloc]initWithAnnotation:routeAnnotation reuseIdentifier:@"start_node"];
                view.image = [UIImage imageWithContentsOfFile:[self getMyBundlePath1:@"images/icon_nav_start.png"]];
                view.centerOffset = CGPointMake(0, -(view.frame.size.height * 0.5));
                view.canShowCallout = TRUE;
            }
            view.annotation = routeAnnotation;
        }
            break;
        case 1:
        {
            view = [mapview dequeueReusableAnnotationViewWithIdentifier:@"end_node"];
            if (view == nil) {
                view = [[BMKAnnotationView alloc]initWithAnnotation:routeAnnotation reuseIdentifier:@"end_node"];
                view.image = [UIImage imageWithContentsOfFile:[self getMyBundlePath1:@"images/icon_nav_end.png"]];
                view.centerOffset = CGPointMake(0, -(view.frame.size.height * 0.5));
                view.canShowCallout = TRUE;
            }
            view.annotation = routeAnnotation;
        }
            break;
        case 2:
        {
            view = [mapview dequeueReusableAnnotationViewWithIdentifier:@"bus_node"];
            if (view == nil) {
                view = [[BMKAnnotationView alloc]initWithAnnotation:routeAnnotation reuseIdentifier:@"bus_node"];
                view.image = [UIImage imageWithContentsOfFile:[self getMyBundlePath1:@"images/icon_nav_bus.png"]];
                view.canShowCallout = TRUE;
            }
            view.annotation = routeAnnotation;
        }
            break;
        case 3:
        {
            view = [mapview dequeueReusableAnnotationViewWithIdentifier:@"rail_node"];
            if (view == nil) {
                view = [[BMKAnnotationView alloc]initWithAnnotation:routeAnnotation reuseIdentifier:@"rail_node"];
                view.image = [UIImage imageWithContentsOfFile:[self getMyBundlePath1:@"images/icon_nav_rail.png"]];
                view.canShowCallout = TRUE;
            }
            view.annotation = routeAnnotation;
        }
            break;
        case 4:
        {
            view = [mapview dequeueReusableAnnotationViewWithIdentifier:@"route_node"];
            if (view == nil) {
                view = [[BMKAnnotationView alloc]initWithAnnotation:routeAnnotation reuseIdentifier:@"route_node"];
                view.canShowCallout = TRUE;
            } else {
                [view setNeedsDisplay];
            }
            
            UIImage* image = [UIImage imageWithContentsOfFile:[self getMyBundlePath1:@"images/icon_direction.png"]];
            view.image = [image imageRotatedByDegrees:routeAnnotation.degree];
            view.annotation = routeAnnotation;
            
        }
            break;
        case 5:
        {
            view = [mapview dequeueReusableAnnotationViewWithIdentifier:@"waypoint_node"];
            if (view == nil) {
                view = [[BMKAnnotationView alloc]initWithAnnotation:routeAnnotation reuseIdentifier:@"waypoint_node"];
                view.canShowCallout = TRUE;
            } else {
                [view setNeedsDisplay];
            }
            
            UIImage* image = [UIImage imageWithContentsOfFile:[self getMyBundlePath1:@"images/icon_nav_waypoint.png"]];
            view.image = [image imageRotatedByDegrees:routeAnnotation.degree];
            view.annotation = routeAnnotation;
        }
            break;
        default:
            break;
    }
    
    return view;
}

- (NSString*)getMyBundlePath1:(NSString *)filename
{
    
    NSBundle * libBundle = MYBUNDLE ;
    if ( libBundle && filename ){
        NSString * s=[[libBundle resourcePath ] stringByAppendingPathComponent : filename];
        return s;
    }
    return nil ;
}

- (void)onGetWalkingRouteResult:(BMKRouteSearch*)searcher result:(BMKWalkingRouteResult*)result errorCode:(BMKSearchErrorCode)error
{
    NSArray* array = [NSArray arrayWithArray:_mapView.annotations];
    [_mapView removeAnnotations:array];
    array = [NSArray arrayWithArray:_mapView.overlays];
    [_mapView removeOverlays:array];
    if (error == BMK_SEARCH_NO_ERROR) {
        BMKWalkingRouteLine* plan = (BMKWalkingRouteLine*)[result.routes objectAtIndex:0];
        NSInteger size = [plan.steps count];
        int planPointCounts = 0;
        for (int i = 0; i < size; i++) {
            BMKWalkingStep* transitStep = [plan.steps objectAtIndex:i];
            if(i==0){
                RouteAnnotation* item = [[RouteAnnotation alloc]init];
                item.coordinate = plan.starting.location;
                item.title = @"起点";
                item.type = 0;
                [_mapView addAnnotation:item]; // 添加起点标注
                
            }else if(i==size-1){
                RouteAnnotation* item = [[RouteAnnotation alloc]init];
                item.coordinate = plan.terminal.location;
                item.title = @"终点";
                item.type = 1;
                [_mapView addAnnotation:item]; // 添加起点标注
            }
            //添加annotation节点
            RouteAnnotation* item = [[RouteAnnotation alloc]init];
            item.coordinate = transitStep.entrace.location;
            item.title = transitStep.entraceInstruction;
            item.degree = transitStep.direction * 30;
            item.type = 4;
            [_mapView addAnnotation:item];
            
            //轨迹点总数累计
            planPointCounts += transitStep.pointsCount;
        }
        
        //轨迹点
        BMKMapPoint * temppoints = new BMKMapPoint[planPointCounts];
        int i = 0;
        for (int j = 0; j < size; j++) {
            BMKWalkingStep* transitStep = [plan.steps objectAtIndex:j];
            int k=0;
            for(k=0;k<transitStep.pointsCount;k++) {
                temppoints[i].x = transitStep.points[k].x;
                temppoints[i].y = transitStep.points[k].y;
                i++;
            }
            
        }
        // 通过points构建BMKPolyline
        BMKPolyline* polyLine = [BMKPolyline polylineWithPoints:temppoints count:planPointCounts];
        [_mapView addOverlay:polyLine]; // 添加路线overlay
        delete []temppoints;
        [self mapViewFitPolyLine:polyLine];
    }
}

//根据polyline设置地图范围
- (void)mapViewFitPolyLine:(BMKPolyline *) polyLine {
    CGFloat ltX, ltY, rbX, rbY;
    if (polyLine.pointCount < 1) {
        return;
    }
    BMKMapPoint pt = polyLine.points[0];
    ltX = pt.x, ltY = pt.y;
    rbX = pt.x, rbY = pt.y;
    for (int i = 1; i < polyLine.pointCount; i++) {
        BMKMapPoint pt = polyLine.points[i];
        if (pt.x < ltX) {
            ltX = pt.x;
        }
        if (pt.x > rbX) {
            rbX = pt.x;
        }
        if (pt.y > ltY) {
            ltY = pt.y;
        }
        if (pt.y < rbY) {
            rbY = pt.y;
        }
    }
    BMKMapRect rect;
    rect.origin = BMKMapPointMake(ltX , ltY);
    rect.size = BMKMapSizeMake(rbX - ltX, rbY - ltY);
    [_mapView setVisibleMapRect:rect];
    _mapView.zoomLevel = _mapView.zoomLevel - 0.3;
}

- (BMKOverlayView*)mapView:(BMKMapView *)map viewForOverlay:(id<BMKOverlay>)overlay
{
    if ([overlay isKindOfClass:[BMKPolyline class]]) {
        BMKPolylineView* polylineView = [[BMKPolylineView alloc] initWithOverlay:overlay];
        polylineView.fillColor = [[UIColor cyanColor] colorWithAlphaComponent:1];
        polylineView.strokeColor = [[UIColor blueColor] colorWithAlphaComponent:0.7];
        polylineView.lineWidth = 3.0;
        return polylineView;
    }
    return nil;
}


#pragma mark - BMKSearchDelegate

- (void)onGetPoiResult:(BMKPoiSearch *)searcher result:(BMKPoiResult*)result errorCode:(BMKSearchErrorCode)error
{
    if (error == BMK_SEARCH_NO_ERROR) {
        [self cleanAllAnnotationAndOverlays];
        [poiResultArray removeAllObjects];
        for (int i = 0; i < result.poiInfoList.count; i++) {
            BMKPoiInfo* poi = [result.poiInfoList objectAtIndex:i];
            redianAnnotation = [[BMKPointAnnotation alloc]init];
            redianAnnotation.coordinate = poi.pt;
            redianAnnotation.title = poi.name;
            [poiResultArray addObject:redianAnnotation];
        }
        // 因为用的全局变量计算，说以要等数组添加好之后
        for (redianAnnotation in poiResultArray) {
            [_mapView addAnnotation:redianAnnotation];  //必须单个的添加
        }

//        [_mapView addAnnotations:poiResultArray];
//        [_mapView showAnnotations:poiResultArray animated:YES];
    } else if (error == BMK_SEARCH_AMBIGUOUS_ROURE_ADDR){
        DLog(@"起始点有歧义");
    } else {
        // 各种情况的判断。。。
    }
}

/**
 *返回suggestion搜索结果
 *@param searcher 搜索对象
 *@param result 搜索结果
 *@param error 错误号，@see BMKSearchErrorCode
 */
- (void)onGetSuggestionResult:(BMKSuggestionSearch*)searcher result:(BMKSuggestionResult*)result errorCode:(BMKSearchErrorCode)error
{
    if (error == BMK_SEARCH_NO_ERROR) {
        [suggestionArray removeAllObjects];
        suggestionArray = [NSMutableArray arrayWithArray:result.keyList];
        if (suggestionArray.count != 0) {
            poiTabelView.hidden = NO;
            [poiTabelView reloadData];
        }
    } else if (error == BMK_SEARCH_AMBIGUOUS_ROURE_ADDR){
        DLog(@"起始点有歧义");
    } else {
        // 各种情况的判断。。。
    }

}


- (void)cleanAnnotation
{
    // 清楚屏幕中所有的annotation
    NSArray* array = [NSArray arrayWithArray:_mapView.annotations];
    [_mapView removeAnnotations:array];
}

- (void)cleanAllAnnotationAndOverlays
{
    NSArray* array = [NSArray arrayWithArray:_mapView.annotations];
    [_mapView removeAnnotations:array];
    array = [NSArray arrayWithArray:_mapView.overlays];
    [_mapView removeOverlays:array];
}

#pragma mark - callOutViewdelegate
- (void)walkGetRoute:(CLLocationCoordinate2D)coordinate
{
    [self queryLineSearch:coordinate];
}
- (void)queryLineSearch:(CLLocationCoordinate2D)coordinate
{
    BMKPlanNode* start = [[BMKPlanNode alloc]init];
    start.pt = baiduUserLocation.location.coordinate;
    BMKPlanNode* end = [[BMKPlanNode alloc]init];
    end.pt = coordinate;
    
    BMKWalkingRoutePlanOption *walkingRouteSearchOption = [[BMKWalkingRoutePlanOption alloc]init];
    walkingRouteSearchOption.from = start;
    walkingRouteSearchOption.to = end;
    BOOL routesearchflag = [routeSearch walkingSearch:walkingRouteSearchOption];
    if(routesearchflag)
    {
        DLog(@"walk检索发送成功");
    }
    else
    {
        DLog(@"walk检索发送失败");
    }
}



#pragma mark - 不显示精度圈以及更改定位图标

- (void)isAccuracyCircleShowNO
{
    BMKLocationViewDisplayParam *displayParam = [[BMKLocationViewDisplayParam alloc]init];
    displayParam.isAccuracyCircleShow = NO;//精度圈是否显示
    ///定位图标名称，需要将该图片放到 mapapi.bundle/images 目录下
//    displayParam.locationViewImgName = @"userLocation@2x.png";
    [_mapView updateLocationViewWithParam:displayParam];
}

#pragma mark - 控制器被销毁的时候释放百度地图

- (void)dealloc {
    if (suggestionSearch != nil) {
        suggestionSearch = nil;
    }
    if (routeSearch != nil) {
        routeSearch = nil;
    }
    if (poiSearch != nil) {
        poiSearch = nil;
    }
    if (_mapView) {
        _mapView = nil;
    }
    DLog(@"dealloc");
}
#pragma mark - 搜索按钮及其方法

- (void)createSpeechSearchView
{
    //  创建语音目的地搜索栏
    speechSearchView=[[UIView alloc] initWithFrame:CGRectMake(20, 70, kScreenWidth-40, 36)];
    speechSearchView.backgroundColor =[UIColor whiteColor];
    //    speechSearchView.layer.cornerRadius=5;
    //    speechSearchView.layer.borderColor=[ColorChangeUtil colorFromHexRGB:@"f2f2f2"].CGColor;
    //    speechSearchView.layer.borderWidth =1.0;
    //    speechSearchView.layer.masksToBounds=YES;
    [self.view addSubview:speechSearchView];
    
    destinationField =[[UITextField alloc] initWithFrame:CGRectMake(20, 3, speechSearchView.frame.size.width-20-20-60, 30)];
    destinationField.placeholder=@"查询目的地";
    destinationField.textAlignment =NSTextAlignmentLeft;
    destinationField.textColor =[UIColor blackColor];
    destinationField.clearButtonMode = UITextFieldViewModeAlways;
    destinationField.delegate =self;
    [destinationField addTarget:self action:@selector(textPOISearch:) forControlEvents:UIControlEventEditingChanged];
    [speechSearchView addSubview:destinationField];
    
    UIImageView *imageview =[[UIImageView alloc] initWithFrame:CGRectMake(speechSearchView.frame.size.width-25-10-10-1, 4, 1, 28)];
    imageview.image=[UIImage imageNamed:@"分割线竖.png"];
    [speechSearchView addSubview:imageview];
    
    UIButton *textSearchBtn =[[UIButton alloc] initWithFrame:CGRectMake(speechSearchView.frame.size.width-60, 0, 61, speechSearchView.frame.size.height)];
    textSearchBtn.backgroundColor = RGB(88, 170, 254);
    [textSearchBtn setTitle:@"搜索" forState:UIControlStateNormal];
    [textSearchBtn addTarget:self action:@selector(searchThePlace) forControlEvents:UIControlEventTouchUpInside];
    [speechSearchView addSubview:textSearchBtn];
    
}
- (void)createSearchBtn
{
    searchBtn =[[UIButton alloc] initWithFrame:CGRectMake(kScreenWidth-50, 70, 36, 36)];
    searchBtn.backgroundColor =[UIColor whiteColor];
    searchBtn.layer.cornerRadius = 18;
    searchBtn.layer.masksToBounds = YES;
    searchBtn.layer.borderColor = RGB(88, 170, 254).CGColor;
    searchBtn.layer.borderWidth = 1.0;
    [searchBtn addTarget:self action:@selector(showSearchView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:searchBtn];
    
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 16, 16)];
    imageview.image = [UIImage imageNamed:@"searchBtn_smaller.jpg"];
    imageview.contentMode = UIViewContentModeScaleToFill;
    imageview.backgroundColor = [UIColor clearColor];
    [searchBtn addSubview:imageview];
    
}


- (void)showSearchView
{
    [self createSpeechSearchView];
    [searchBtn removeFromSuperview];
}

- (void)textPOISearch:(UITextField*)textField
{
    // 自建搜索框，用中文拼音输入法时，输入拼音，尚未选定具体字符时，如何使uitextfield不把输入的拼音认作文本编辑框的内容？
    //
//    UITextRange *selectedRange = [textfield markedTextRange];
//    NSString * newText = [textfield textInRange:selectedRange];    //获取高亮部分
//    if(newText.length>0)
//        return;
    [suggestionArray removeAllObjects];
    [poiTabelView reloadData];
    poiTabelView.hidden = YES;
    if (!textField.markedTextRange) {
//        [self poiSearchCity:@"苏州" andkeyword:textField.text];
        [self suggestionSearchCity:@"苏州" andkeyword:textField.text];
    }
}

- (void)poiSearchCity:(NSString *)city andkeyword:(NSString *)keyword
{
    BMKCitySearchOption *citySearchOption = [[BMKCitySearchOption alloc]init];
    //        citySearchOption.pageIndex = curPage; // poi每次返回10个数据
    //        citySearchOption.pageCapacity = 10;
    citySearchOption.city= city;
    citySearchOption.keyword = keyword;
    BOOL flag = [poiSearch poiSearchInCity:citySearchOption];
    if(flag)
    {
        
        DLog(@"城市内检索发送成功");
        [speechSearchView removeFromSuperview];
        [self createSearchBtn];
    }
    else
    {
        // 提醒失败
        DLog(@"城市内检索发送失败");
    }

}

- (void)suggestionSearchCity:(NSString *)city andkeyword:(NSString *)keyword
{
    BMKSuggestionSearchOption *suggestionSearchOption = [[BMKSuggestionSearchOption alloc] init];
    suggestionSearchOption.cityname = city;
    suggestionSearchOption.keyword = keyword;
    BOOL flag = [suggestionSearch suggestionSearch:suggestionSearchOption];
    if (flag) {
        DLog(@"城市内建议检索发送成功");
    }
    else
    {
        DLog(@"城市内建议检索发送失败");
    }
}

- (void)searchThePlace
{
    // 判断网络状况
    if ([CheckNetWork isNetWorkEnable]) {
        // 去除空格
        NSString* str = [destinationField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (destinationField.text == nil||[str isEqualToString:@""]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"请输入关键字" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
        else
        {
//            // 移除地理提示框
//            if (poiTabelView) {
//                [poiTabelView removeFromSuperview];
//                poiTabelView = nil;
//                [suggestionArray removeAllObjects];
//            }
//            _mapView.userTrackingMode = BMKUserTrackingModeNone;//设置定位的状态
//            [self poiSearchCity:@"苏州" andkeyword:destinationField.text];
            [self searchThePlace:destinationField.text];
        }

    }
    else
    {
        // 提示当前无网络连接
    }

}

- (void)searchThePlace:(NSString *)keyWord
{
    // 移除地理提示框
    if (poiTabelView) {
        [poiTabelView removeFromSuperview];
        poiTabelView = nil;
        [suggestionArray removeAllObjects];
    }
    _mapView.userTrackingMode = BMKUserTrackingModeNone;//设置定位的状态
    [self poiSearchCity:@"苏州" andkeyword:keyWord];
}

#pragma mark - textFielddelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    // 创建poi列表
    if (!poiTabelView) {
        poiTabelView =[[UITableView alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(speechSearchView.frame), CGRectGetWidth(speechSearchView.frame), 150)];
        poiTabelView.rowHeight = 50;
        poiTabelView.dataSource = self;
        poiTabelView.delegate = self;
        poiTabelView.tag = 101;
        poiTabelView.showsVerticalScrollIndicator = NO;
        poiTabelView.bounces = NO;
        poiTabelView.hidden = YES;// 弹簧效果
        [self.view addSubview:poiTabelView];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    // 去除空格
    NSString* str = [destinationField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    // 效果，要求无任何输入的时候，点击return键，搜索框消失，搜索按钮显示
    if (textField.text.length == 0 || [str isEqualToString:@""] || textField.text == nil)
    {
        // 移除地理提示框
        if (poiTabelView) {
            [poiTabelView removeFromSuperview];
            poiTabelView = nil;
            [suggestionArray removeAllObjects];
        }
        
        [speechSearchView removeFromSuperview];
        
        [self createSearchBtn];
    
    }
    // 效果，要求有输入的时候，点击return键，键盘弹回去
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - tableviwdelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger number = 0;
    if (tableView.tag == 102) {
        number = moreFunctionArray.count;
    }else if (tableView.tag == 101)
    {
        number = suggestionArray.count;
    }
    return number;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (tableView.tag == 101) {
        static NSString * poiCellIdentifier = @"poiListCell";
        cell = [tableView dequeueReusableCellWithIdentifier:poiCellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:poiCellIdentifier];
        }
        cell.textLabel.text = suggestionArray[indexPath.row];
        return cell;
    }
    else if(tableView.tag == 102)
    {
        static NSString * moreFunctionIdentifier = @"moreFunctionCell";
        cell = [tableView dequeueReusableCellWithIdentifier:moreFunctionIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:moreFunctionIdentifier];
        }
        cell.textLabel.text = moreFunctionArray[indexPath.row];
//        UIView *backView = [[UIView alloc] initWithFrame:cell.frame];
//        backView.backgroundColor = RGB(10, 14, 39);
//        cell.selectedBackgroundView = backView;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 101) {
        NSString *keyWord = suggestionArray[indexPath.row];
        [self searchThePlace:keyWord];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat hight;
    if (tableView.tag == 102) {
        hight = 40;
    }
    else if (tableView.tag == 101)
    {
        return 50;
    }
    return hight;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
