//
//  AppDelegate.m
//  仿QQ抽屉效果
//
//  Created by zhang_ycai on 15-9-28.
//  Copyright (c) 2015年 zyc. All rights reserved.
//

#import "AppDelegate.h"
#import "LeftSlideViewController.h"
#import <ShareSDK/ShareSDK.h>
#import <ShareSDKConnector/ShareSDKConnector.h>
#import <BaiduMapAPI_Map/BMKMapComponent.h>
#import "WXApi.h"

BMKMapManager* _mapManager;
@interface AppDelegate ()
<BMKGeneralDelegate>
{
    NSUserDefaults * user; // 本地用户数据
}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // IP
    [self setIP];
    [self addShareSDK];
    
    // 百度地图
    [self initBaiDuMap];
    // window
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    
   
    
    LeftSlideViewController *leftSlideViewController = [[LeftSlideViewController alloc] initWithBackgroundimage:nil];
    
    self.window.rootViewController = leftSlideViewController;
    
    
    [self.window makeKeyAndVisible];
    return YES;
}

#pragma mark - ip
- (void)setIP
{
    user = [NSUserDefaults standardUserDefaults];
    //   [user setObject:@"58.221.239.152:8014" forKey:@"IP"]; // 现场WebService 地址
    //   [user setObject:@"58.221.239.152:8011" forKey:@"IP"]; // 现场WebService 地址
//    [user setObject:@"192.168.132.59:8080" forKey:@"IP"]; //陈帝
    
    //   [user setObject:@"192.168.128.144:8080" forKey:@"IP"]; // 测试
}
#pragma mark - shareSDK配置
- (void)addShareSDK
{
    /**
     *  设置ShareSDK的appKey，如果尚未在ShareSDK官网注册过App，请移步到http://mob.com/login 登录后台进行应用注册，
     *  在将生成的AppKey传入到此方法中。
     *  方法中的第二个参数用于指定要使用哪些社交平台，以数组形式传入。第三个参数为需要连接社交平台SDK时触发，
     *  在此事件中写入连接代码。第四个参数则为配置本地社交平台时触发，根据返回的平台类型来配置平台信息。
     *  如果您使用的时服务端托管平台信息时，第二、四项参数可以传入nil，第三项参数则根据服务端托管平台来决定要连接的社交SDK。
     */
    [ShareSDK registerApp:@"240dde942324"
          activePlatforms:@[@(SSDKPlatformTypeSinaWeibo),@(SSDKPlatformTypeTencentWeibo),@(SSDKPlatformTypeWechat)]
                 onImport:^(SSDKPlatformType platformType)
     {
         switch (platformType) {
             case SSDKPlatformTypeWechat:
                 [ShareSDKConnector connectWeChat:[WXApi class]];
                 break;
                 
             default:
                 break;
         }
     }
          onConfiguration:^(SSDKPlatformType platformType, NSMutableDictionary *appInfo) {
              switch (platformType) {
                  case SSDKPlatformTypeSinaWeibo:
                      [appInfo SSDKSetupSinaWeiboByAppKey:@"1718980243"
                                            appSecret:@"452d0724db4b9eb607541ef84fe48a22"
                                              redirectUri:@"http://www.cvicse.com" authType:SSDKAuthTypeBoth];
//                      //设置新浪微博应用信息,其中authType设置为使用SSO＋Web形式授权
//                      break;
//                  case SSDKPlatformTypeTencentWeibo:
//                      [appInfo SSDKSetupTencentWeiboByAppKey:@"801521701" appSecret:@"6a798a3ba579c7853fa069bb5322caf6" redirectUri:@"http://www.baidu.com"];
//                  case SSDKPlatformTypeWechat:
//                      [appInfo SSDKSetupWeChatByAppId:@"wx9c54d714b66ab455"
//                                            appSecret:@""];
                  default:
                      break;
              }
          }];
}
#pragma mark - baiduMap

- (void)initBaiDuMap
{
    // 要使用百度地图，请先启动BaiduMapManager
    _mapManager = [[BMKMapManager alloc]init];
    BOOL ret = [_mapManager start:@"s3GUp79OFsAXDUZeOjIRzNYo" generalDelegate:self];
    if (!ret) {
        NSLog(@"manager start failed!");
    }
    // 在使用Xcode6进行SDK开发过程中，需要在info.plist中添加：Bundle display name ，且其值不能为空（Xcode6新建的项目没有此配置，若没有会造成manager start failed）
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
