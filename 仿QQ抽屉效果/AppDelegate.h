//
//  AppDelegate.h
//  仿QQ抽屉效果
//
//  Created by zhang_ycai on 15-9-28.
//  Copyright (c) 2015年 zyc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

// 为什么.h文件和.m文件里各有1个@interface?它们分别有什么用？

//.h里面的@interface，不消说，是典型的头文件，它是供其它Class调用的。它的@property和functions，都能够被其它Class“看到”。

//而.m里面的@interface，在OC里叫作Class Extension，是.h文件中@interface的补充。但是.m文件里的@interface，对外是不开放的，只在.m文件里可见。

//因此，我们将对外开放的方法、变量放到.h文件中，而将不想要对外开放的变量放到.m文件中（.m文件的方法可以不声明，直接用）。
/*
// git 使用
 git add 添加文件到缓存区 git add .
 git status 查看文件状态
 git commit -m ""
 git log 查看版本
 git relog 查看简介版本
 // 账号 Jayce
 git push -u origin master
 账号 qq号
 密码 社区密码
 --hard HEAD 回退到当前版本
 --hard HEAD ^ 回退到上一个版本
 --hard HEAD ^^
 --hard HEAD ~100
 --hard 版本号前7位
*/

