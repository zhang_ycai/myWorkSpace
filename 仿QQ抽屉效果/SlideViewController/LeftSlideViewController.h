//
//  LeftSlideViewController.h
//  仿QQ抽屉效果
//
//  Created by zhang_ycai on 15-9-28.
//  Copyright (c) 2015年 zyc. All rights reserved.
//

#define kScreenSize [[UIScreen mainScreen] bounds].size


#define kMainPageDistance   100  // 弹出侧边栏时，主页露出的宽度
#define kMainPageScale      0.8  // 弹出侧边栏时，主页面的缩放比例
#define kMainPageCenter     CGPointMake(kScreenWidth + \
        kScreenWidth      * kMainPageScale / 2.0 - kMainPageDistance, \
        kScreenHeight / 2)       // 弹出侧边栏时，主页面中心点

#define kDefualtDistance    (kScreenWidth - kMainPageDistance) / 2.0 - 40 // 滑动距离大于此数时，状态改变（关--》开，或者开--》关）
#define kSpeedFloat         0.5  // 滑动速度

#define kLeftAlpha          0.9  // 侧边栏蒙版的最大值
#define kLeftCenterX        30   // 侧边栏初始偏移量
#define kLeftScale          0.7  // 边栏初始缩放比例

#define vDeckCanNotPanViewTag 1


#import <UIKit/UIKit.h>
#import "YCNavigationViewController.h"

@interface LeftSlideViewController : UIViewController


// 左侧控制器
@property (nonatomic, strong) UIViewController *leftViewController;
// 右侧控制器
@property (nonatomic, strong) UIViewController *mainViewController;
// 导航控制器
@property (nonatomic, strong) YCNavigationViewController *mainNav;
// 点击手势，是否允许点击视图恢复视图位置。默认为yes
@property (nonatomic, strong) UITapGestureRecognizer *tap;
// 滑动手势控制器，默认yes
@property (nonatomic, strong) UIPanGestureRecognizer *pan;
// 轻扫手势
@property (nonatomic, strong) UISwipeGestureRecognizer *swipe;
// 侧滑窗是否关闭(关闭时显示为主页)
@property (nonatomic, assign) BOOL closed;
// 滑动系数
@property (nonatomic, assign) CGFloat speedf;

@property (nonatomic, assign) NSMutableArray *leftTitleArray;

@property (nonatomic, assign) NSMutableArray *leftControllerArray;

// 初始化方法
- (instancetype)initWithBackgroundimage:(UIImage *)image;
// 隐藏菜单
- (void)hideLeftView;

// 显示菜单
- (void)showLeftView;

// 是否可以侧滑, 默认为yes
- (void)setPanEnabled: (BOOL) enabled;

@end
