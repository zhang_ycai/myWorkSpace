//
//  LeftSlideViewController.m
//  仿QQ抽屉效果
//
//  Created by zhang_ycai on 15-9-28.
//  Copyright (c) 2015年 zyc. All rights reserved.
//

#import "LeftSlideViewController.h"
#import "MainViewController.h"
#import "YCLeftViewController.h"
#import "ViewController.h" // 测试
#import <ShareSDK/ShareSDK.h>
#import <ShareSDKExtension/SSEShareHelper.h>
#import <ShareSDKUI/ShareSDK+SSUI.h>
#import <ShareSDKUI/SSUIShareActionSheetStyle.h>
#import <ShareSDKUI/SSUIShareActionSheetCustomItem.h>
#import <ShareSDK/ShareSDK+Base.h>

#import <ShareSDKExtension/ShareSDK+Extension.h>


@interface LeftSlideViewController ()<UIGestureRecognizerDelegate,MainViewControllerdelegate,NavigationVCDelegate,LeftViewControllerdelegate>

// 侧边栏蒙版
@property (nonatomic, strong) UIView *contentView;

@end

@implementation LeftSlideViewController
{
    CGFloat _mainScalef; // 主页面的实时位移
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // 控制器数组
    // Do any additional setup after loading the view.
}

// 初始化
- (instancetype)initWithBackgroundimage:(UIImage *)image
{
    if (self == [super init]) {
        // 初始化侧边栏和主页面
        MainViewController *mainVC = [[MainViewController alloc] init];
        YCLeftViewController *leftVC = [[YCLeftViewController alloc] init];
        
        leftVC.delegate = self;
        
        self.mainNav = [[YCNavigationViewController alloc] initWithRootViewController:mainVC];
        self.mainNav.Delegate = self;

        self.leftViewController = leftVC;
        self.mainViewController = self.mainNav;
        mainVC.delegate = self;
        
        // 滑动速度
        self.speedf = kSpeedFloat;
        
        // 背景图片
        if (image  == nil) {
            //
            self.view.backgroundColor = [UIColor greenColor];
        }else
        {
            UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
            imageView.frame = self.view.bounds;
            [self.view addSubview:imageView];
        }
        
        
        // 侧边栏
        self.leftViewController.view.backgroundColor = [UIColor clearColor];
        self.leftViewController.view.frame =
             CGRectMake(0,
                        0,
                        kScreenWidth - kMainPageDistance,
                        kScreenHeight);
        self.leftViewController.view.transform =
             CGAffineTransformMakeScale(kMainPageScale,
                                        kMainPageScale);
        self.leftViewController.view.center = CGPointMake(kLeftCenterX, kScreenHeight*0.5);
        [self.view addSubview:self.leftViewController.view];
        self.leftViewController.view.hidden = YES;
        
        // 先添加侧边栏，在添加蒙版，侧边栏就在蒙版的下面
        // 侧边栏蒙版
        UIView *view = [[UIView alloc] init];
        view.frame = self.view.frame;
        view.backgroundColor = [UIColor blackColor];
        view.alpha = 0.4f;
        self.contentView = view;
        [self.view addSubview:view];
        
        /*
         *注意：
         *添加自定义手势时，必须设置UIGestureRecognizer的属性cancelsTouchesInView 和 delaysTouchesEnded 为NO,
         *否则影响地图内部的手势处理
         */

        // 侧滑手势
        self.pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handPan:)];
        self.pan.cancelsTouchesInView = NO;
        self.pan.delaysTouchesEnded = NO;
        [self.mainViewController.view addGestureRecognizer:self.pan];
        
        [self.pan setCancelsTouchesInView:YES];
        self.pan.delegate = self;
        // 轻扫手势
        self.swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handSwipe:)];
        self.swipe.cancelsTouchesInView = NO;
        self.swipe.delaysTouchesEnded = NO;
        [self.mainNav.view addGestureRecognizer:self.swipe];
        
        //
        [self.view addSubview:self.mainViewController.view];
        self.closed = YES;

    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.leftViewController.view.hidden = NO;
}

- (void)handPan:(UIPanGestureRecognizer *)pan
{
    CGPoint point = [pan translationInView:self.view]; // 获取当前视图的位置
    _mainScalef = (point.x * self.speedf + _mainScalef); // 计算出滑动距离同时可以判断方向
    
    BOOL needMoveWithTap = YES;  // 是否还需要跟随手指移动

    if (((self.mainViewController.view.frame.origin.x <= 0) && (_mainScalef <= 0)) || ((self.mainViewController.view.frame.origin.x >= (kScreenWidth - kMainPageDistance )) && (_mainScalef >= 0)))
    {
        //边界值管控
        _mainScalef = 0;
        needMoveWithTap = NO;

    }
    //根据视图位置判断是左滑还是右边滑动
    if (needMoveWithTap && (pan.view.frame.origin.x >= 0) && (pan.view.frame.origin.x <= (kScreenWidth - kMainPageDistance)))
    {
        CGFloat panCenterX = pan.view.center.x + point.x * self.speedf;
        if (panCenterX < kScreenWidth * 0.5 - 2) // 移动到窗口时不能向左移动
        {
            panCenterX = kScreenWidth * 0.5;
        }
        CGFloat panCenterY = pan.view.center.y;
        
        pan.view.center = CGPointMake(panCenterX, panCenterY); // 主页面跟随
        
        CGFloat scale = 1 - (1 - kMainPageScale) * (pan.view.frame.origin.x / (kScreenWidth - kMainPageDistance)); // 计算出主页面的缩放系数
        pan.view.transform = CGAffineTransformScale(CGAffineTransformIdentity, scale, scale);
        [pan setTranslation:CGPointMake(0, 0) inView:self.view];
        
        // 侧边栏是否缩小
        CGFloat leftTabCenterX = kLeftCenterX + ((kScreenWidth - kMainPageDistance) * 0.5 - kLeftCenterX) * (pan.view.frame.origin.x / (kScreenWidth - kMainPageDistance)); // 侧边栏随手势的位置
        CGFloat leftScale = kLeftScale + (1 - kLeftScale) * (pan.view.frame.origin.x / (kScreenWidth - kMainPageDistance)); // 侧边栏随手势的系数
        
        self.leftViewController.view.center = CGPointMake(leftTabCenterX, kScreenHeight * 0.5);
        self.leftViewController.view.transform = CGAffineTransformScale(CGAffineTransformIdentity, leftScale,leftScale);
         
        // 随着手势改变蒙版的透明度
        //tempAlpha kLeftAlpha~0
        CGFloat tempAlpha = kLeftAlpha - kLeftAlpha * (pan.view.frame.origin.x / (kScreenWidth - kMainPageDistance));
        self.contentView.alpha = tempAlpha;

    }
    else
    {
        //超出范围，
        if (self.mainViewController.view.frame.origin.x < 0)
        {
            [self hideLeftView];
            _mainScalef = 0;
        }
        else if (self.mainViewController.view.frame.origin.x > (kScreenWidth - kMainPageDistance))
        {
            [self showLeftView];
            _mainScalef = 0;
        }

    }
    
    //手势结束后修正位置,超过约一半时向多出的一半偏移
    if (pan.state == UIGestureRecognizerStateEnded) { // 判断手势是否结束
        if (fabs(_mainScalef) > 0) // 判断移动的距离是否大于默认位置 kDefualtDistance
            // 设置为零则移动就侧滑
        {
            if (self.closed)
            {
                [self showLeftView];
            }
            else
            {
                [self hideLeftView];
            }
        }
        else
        {
            if (self.closed)
            {
                [self hideLeftView];
            }
            else
            {
                [self showLeftView];
            }
        }
        _mainScalef = 0;  // 清零
    }

}

- (void)handSwipe:(UISwipeGestureRecognizer *)swipe
{
    
    if (self.swipe.direction==UISwipeGestureRecognizerDirectionRight) {
        [self.mainNav popViewControllerAnimated:YES];
    }
}

#pragma mark - 修改视图位置
/**
 @brief 关闭侧边栏
 */
- (void)hideLeftView
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    self.mainViewController.view.transform = CGAffineTransformScale(CGAffineTransformIdentity,1.0,1.0);
    self.mainViewController.view.center = CGPointMake(kScreenWidth / 2, kScreenHeight / 2);
    self.closed = YES;
    
    self.leftViewController.view.center = CGPointMake(kLeftCenterX, kScreenHeight * 0.5);
    self.leftViewController.view.transform = CGAffineTransformScale(CGAffineTransformIdentity,kLeftScale,kLeftScale);
    self.contentView.alpha = kLeftAlpha;
    
    [UIView commitAnimations];
    [self removeSingleTap];
}


/**
 @brief 打开侧边栏
 */
- (void)showLeftView;
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    self.mainViewController.view.transform = CGAffineTransformScale(CGAffineTransformIdentity,kMainPageScale,kMainPageScale);
    self.mainViewController.view.center = kMainPageCenter;
    self.closed = NO;
    
    self.leftViewController.view.center = CGPointMake((kScreenWidth - kMainPageDistance) * 0.5, kScreenHeight * 0.5);
    self.leftViewController.view.transform = CGAffineTransformScale(CGAffineTransformIdentity,1.0,1.0);
    self.contentView.alpha = 0;
    
    [UIView commitAnimations];
    [self disableTapButton];
}

#pragma mark - 行为收敛控制
- (void)disableTapButton
{
    for (UIButton *tempButton in [_mainViewController.view subviews])
    {
        [tempButton setUserInteractionEnabled:NO];
    }
    //单击
    if (!self.tap)
    {
        //单击手势
        self.tap= [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handeTap:)];
        [self.tap setNumberOfTapsRequired:1];
        
        [self.mainViewController.view addGestureRecognizer:self.tap];
        self.tap.cancelsTouchesInView = YES;  //点击事件盖住其它响应事件,但盖不住Button;
    }
}
//关闭行为收敛
- (void) removeSingleTap
{
    for (UIButton *tempButton in [self.mainViewController.view  subviews])
    {
        [tempButton setUserInteractionEnabled:YES];
    }
    [self.mainViewController.view removeGestureRecognizer:self.tap]; // 移除点击手势
    self.tap = nil;
}
#pragma mark - 单击手势
-(void)handeTap:(UITapGestureRecognizer *)tap{
    
    if ((!self.closed) && (tap.state == UIGestureRecognizerStateEnded))
    {
        [self hideLeftView];
        _mainScalef = 0;
    }
    
}

/**
 *  设置滑动开关是否开启
 *
 *  @param enabled YES:支持滑动手势，NO:不支持滑动手势
 */

- (void)setPanEnabled: (BOOL) enabled
{
    [self.pan setEnabled:enabled];
}

- (void)setSwipeEnabled:(BOOL)enabled
{
    [self.swipe setEnabled:enabled];
}


#pragma mark - 主页面回调函数
- (void)leftBtnClicked
{
    [self showLeftView];
}

- (void)push
{
    if (!self.closed) {
        [self hideLeftView];
    }
    [self setPanEnabled:NO];
    [self setSwipeEnabled:YES];
}

- (void)pop
{
    [self setPanEnabled:YES];
    [self setSwipeEnabled:NO];
}
#pragma mark - 侧边栏对应控制器

- (NSMutableArray *)leftControllerArray
{
    if (_leftControllerArray == nil) {
        _leftControllerArray = [[NSMutableArray alloc] init];
    }
    return _leftControllerArray;
}

- (NSMutableArray *)leftTitleArray
{
    if (_leftTitleArray == nil) {
        _leftTitleArray = [[NSMutableArray alloc] init];
    }
    return _leftTitleArray;
}

- (void)setLeftController
{
    _leftTitleArray = [[NSMutableArray alloc] initWithArray:@[@"个人中心", @"我的消息", @"意见反馈", @"关于我们", @"离线下载地图", @"帮助", @"分享"]];
    _leftControllerArray = [[NSMutableArray alloc] initWithArray:@[@"", @"", @"", @"", @"", @"", @"ShareViewCo"]];
}
- (void)didSelectItem:(NSString *)title
{
    if ([title isEqualToString:@"分享"]) {
        [self share];
    }else
    {
        // 测试控制器
        ViewController *test = [[ViewController alloc] init];
        [self.mainNav pushViewController:test animated:YES];
    }
}

- (void)share
{
    NSMutableDictionary* shareParams = [NSMutableDictionary dictionary];
    [shareParams SSDKSetupShareParamsByText:@"逗你玩"
                                     images:[UIImage imageNamed:@"畅行南通.png"]
                                        url:[NSURL URLWithString:@"http://baidu.com"]
                                      title:@"=。='" type:SSDKContentTypeText];
    //进行分享
//    [ShareSDK share:SSDKPlatformTypeSinaWeibo
//         parameters:shareParams
//     onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
//         
//         switch (state) {
//             case SSDKResponseStateSuccess:
//             {
//                 UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"分享成功"
//                                                                     message:nil
//                                                                    delegate:nil
//                                                           cancelButtonTitle:@"确定"
//                                                           otherButtonTitles:nil];
//                 [alertView show];
//                 break;
//             }
//             case SSDKResponseStateFail:
//             {
//                 UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"分享失败"
//                                                                     message:[NSString stringWithFormat:@"%@", error]
//                                                                    delegate:nil
//                                                           cancelButtonTitle:@"确定"
//                                                           otherButtonTitles:nil];
//                 [alertView show];
//                 break;
//             }
//             case SSDKResponseStateCancel:
//             {
//                 UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"分享已取消"
//                                                                     message:nil
//                                                                    delegate:nil
//                                                           cancelButtonTitle:@"确定"
//                                                           otherButtonTitles:nil];
//                 [alertView show];
//                 break;
//             }
//             default:
//                 break;
//         }
//         
//     }];
    [ShareSDK showShareActionSheet:self.view
                             items:@[@(SSDKPlatformTypeWechat),@(SSDKPlatformTypeSinaWeibo)]
                       shareParams:shareParams
               onShareStateChanged:^(SSDKResponseState state, SSDKPlatformType platformType, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error, BOOL end) {
                   
                   switch (state) {
                           
                       case SSDKResponseStateBegin:
                       {
//                           [theController showLoadingView:YES];
                           break;
                       }
                       case SSDKResponseStateSuccess:
                       {
                           UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"分享成功"
                                                                               message:nil
                                                                              delegate:nil
                                                                     cancelButtonTitle:@"确定"
                                                                     otherButtonTitles:nil];
                           [alertView show];
                           break;
                       }
                       case SSDKResponseStateFail:
                       {
                           if (platformType == SSDKPlatformTypeSMS && [error code] == 201)
                           {
                               UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"分享失败"
                                                                               message:@"失败原因可能是：1、短信应用没有设置帐号；2、设备不支持短信应用；3、短信应用在iOS 7以上才能发送带附件的短信。"
                                                                              delegate:nil
                                                                     cancelButtonTitle:@"OK"
                                                                     otherButtonTitles:nil, nil];
                               [alert show];
                               break;
                           }
                           else if(platformType == SSDKPlatformTypeMail && [error code] == 201)
                           {
                               UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"分享失败"
                                                                               message:@"失败原因可能是：1、邮件应用没有设置帐号；2、设备不支持邮件应用；"
                                                                              delegate:nil
                                                                     cancelButtonTitle:@"OK"
                                                                     otherButtonTitles:nil, nil];
                               [alert show];
                               break;
                           }
                           else
                           {
                               UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"分享失败"
                                                                               message:[NSString stringWithFormat:@"%@",error]
                                                                              delegate:nil
                                                                     cancelButtonTitle:@"OK"
                                                                     otherButtonTitles:nil, nil];
                               [alert show];
                               break;
                           }
                           break;
                       }
                       case SSDKResponseStateCancel:
                       {
                           UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"分享已取消"
                                                                               message:nil
                                                                              delegate:nil
                                                                     cancelButtonTitle:@"确定"
                                                                     otherButtonTitles:nil];
                           [alertView show];
                           break;
                       }
                       default:
                           break;
                   }
                   }];
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer*)gestureRecognizer shouldReceiveTouch:(UITouch*)touch {
    
    if(touch.view.tag == vDeckCanNotPanViewTag)
    {
        //        NSLog(@"不响应侧滑");
        return NO;
    }
    else
    {
        //        NSLog(@"响应侧滑");
        return YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
