//
//  CheckNetWork.h
//  TicketProject
//
//  Created by sls002 on 13-7-31.
//  Copyright (c) 2013年 sls002. All rights reserved.
//


/* 判断是否有网络
 * 使用方法[CheckNetWork isNetWorkEnable]
 */

#import <Foundation/Foundation.h>

@interface CheckNetWork : NSObject

+(BOOL)isNetWorkEnable;

@end
