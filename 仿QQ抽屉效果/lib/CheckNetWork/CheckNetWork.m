//
//  CheckNetWork.m
//  TicketProject
//
//  Created by sls002 on 13-7-31.
//  Copyright (c) 2013年 sls002. All rights reserved.
//

#import "CheckNetWork.h"
#import "Reachabilityly.h"

@implementation CheckNetWork

+(BOOL)isNetWorkEnable
{
    if(([Reachabilityly reachabilityForInternetConnection].currentReachabilityStatus == NotReachable)
       && ([Reachabilityly reachabilityForLocalWiFi].currentReachabilityStatus == NotReachable))
    {
        return NO;
    }
    else
    {
        return YES;
    }
}


@end
