//
//  NoNetWorkAlert.m
//  畅行南通
//
//  Created by ge_cming on 15-12-8.
//  Copyright (c) 2015年 date. All rights reserved.
//

#import "NoNetWorkAlert.h"

@implementation NoNetWorkAlert
+ (void)addNoNetWorkAlert
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"数据连接超时或网络错误" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    
    [alert show];
}
@end
