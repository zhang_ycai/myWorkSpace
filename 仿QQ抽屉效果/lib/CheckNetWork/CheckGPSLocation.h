//
//  CheckGPSLocation.h
//  畅行南通
//
//  Created by 海一长虹项目 on 15/12/13.
//  Copyright (c) 2015年 date. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BaiduMapAPI_Map/BMKMapComponent.h> 

/*
 * 百度地图的GPS监测方法
 */

/*
 * 使用条件，UIAlertViewDelegate,使用页面无alert 代理方法冲突
 */


@interface CheckGPSLocation : NSObject
+(BOOL)locationServicesEnabledanddelegate:(nullable id)delegate;//检查GPS定位是否开启；
@end
