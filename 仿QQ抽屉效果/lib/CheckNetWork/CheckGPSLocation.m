//
//  CheckGPSLocation.m
//  畅行南通
//
//  Created by 海一长虹项目 on 15/12/13.
//  Copyright (c) 2015年 date. All rights reserved.
//

#import "CheckGPSLocation.h"

@implementation CheckGPSLocation
+(BOOL)locationServicesEnabledanddelegate:(id)delegate
{
    if (IOS8_OR_LATER) {
        // 8.0之后 分为三种模式
        if ([CLLocationManager locationServicesEnabled] && [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) {
            // 使用期间
            DLog(@"手机GPS定位已经开启");
            return YES;
        } else if ([CLLocationManager locationServicesEnabled] && [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways) {
            // 始终
            DLog(@"手机GPS定位已经开启");
            return YES;
        } else {
            DLog(@"手机GPS定位未开启");
            UIAlertView *locationAlert =[[UIAlertView alloc]initWithTitle:@"提醒" message:@"请开启定位服务设置" delegate:delegate cancelButtonTitle:@"设置" otherButtonTitles:@"取消", nil];
            locationAlert.tag = 10086;
            [locationAlert show];
            return NO;
        }
        
    }else{
        
        if ([CLLocationManager locationServicesEnabled]) {
            // 地位服务是否开启
            DLog(@"手机GPS定位已经开启");
            return YES;
        }else{
            DLog(@"手机GPS定位未开启");
            UIAlertView *locationAlert =[[UIAlertView alloc]initWithTitle:@"提醒" message:@"请开启定位服务设置" delegate:delegate cancelButtonTitle:@"设置" otherButtonTitles:@"取消", nil];
            locationAlert.tag = 10086;
            [locationAlert show];
            return NO;
        }
        
    }
 
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 10086) {
        if (buttonIndex == 0) {
            // 定位设置URL
            NSURL *url = [NSURL URLWithString:@"prefs:root=LOCATION_SERVICES"];
            [[UIApplication sharedApplication]openURL:url];
        }
    }
}

@end
