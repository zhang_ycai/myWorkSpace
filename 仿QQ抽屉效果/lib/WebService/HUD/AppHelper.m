//
//  AppHelper.m
//  WebService
//
//  Created by aJia on 12/12/24.
//  Copyright (c) 2012年 rang. All rights reserved.
//

#import "AppHelper.h"
#import "MBProgressHUD_su.h"
@implementation AppHelper

//static NSTimer * timer;
static MBProgressHUD_su *HUD;
static bool yesorno;
//MBProgressHUD 的使用方式，只对外两个方法，可以随时使用(但会有警告！)，其中窗口的 alpha 值 可以在源程序里修改。
+ (void)showHUD:(NSString *)msg{
	yesorno = YES;
	HUD = [[MBProgressHUD_su alloc] initWithWindow:[UIApplication sharedApplication].keyWindow];
	[[UIApplication sharedApplication].keyWindow addSubview:HUD];
    HUD.dimBackground = YES;
	HUD.labelText = msg;
	[HUD show:YES];
//  timer = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(timestr) userInfo:nil repeats:YES];
}
//-(void)timestr{
//    [HUD hide:YES];
//	[HUD removeFromSuperViewOnHide];
//	[HUD release];
//    [timer invalidate];
//    NSLog(@"关闭了");
//}
+ (void)removeHUD{
	if (yesorno) {
        [HUD hide:YES];
        [MBProgressHUD_su hideAllHUDsForView:[UIApplication sharedApplication].keyWindow animated:YES];
        //	[HUD removeFromSuperViewOnHide];
        [HUD release];
        yesorno=NO;
    }
//    NSLog(@"MBProgressHUD移除");
	
}

@end
