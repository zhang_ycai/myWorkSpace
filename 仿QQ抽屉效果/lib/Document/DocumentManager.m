//
//  DocumentManager.m
//  qq
//
//  Created by cvicseks-mac1 on 16/2/23.
//  Copyright © 2016年 zyc. All rights reserved.
//

#import "DocumentManager.h"

@implementation DocumentManager
{
    NSFileManager *_YC_NSFileManager;
}

static DocumentManager *YC_DocumentManager;
+ (instancetype)ManagerShareInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        YC_DocumentManager = [[DocumentManager alloc] init];
    });
    return YC_DocumentManager;
}
- (instancetype)init
{
    if(self = [super init]){
    
        _YC_NSFileManager = [[NSFileManager alloc] init];
    }
    return self;
}

- (NSString *)getDocumentPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [paths objectAtIndex:0];
}

- (NSString *)getDocumentPath:(NSString *)fileName
{
    return [[self getDocumentPath] stringByAppendingPathComponent:fileName];
}

- (BOOL)isDocumentFileExists:(NSString *)fileName
{
    return [_YC_NSFileManager fileExistsAtPath:[self getDocumentPath:fileName]];
}

- (void)createDocumentFileName:(NSString *)fileName type:(NSString *)fileType
{
    NSError *error;
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:fileType];
    if (![self isDocumentFileExists:path]) {
        NSString *destinationPath = [[self getDocumentPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@",fileName,fileType]];
        [_YC_NSFileManager createDirectoryAtPath:destinationPath withIntermediateDirectories:YES attributes:nil error:&error];
    }
    else
    {
        DLog("文件已存在");
    }
}

@end
