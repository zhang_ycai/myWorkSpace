//
//  DocumentManager.h
//  qq
//
//  Created by cvicseks-mac1 on 16/2/23.
//  Copyright © 2016年 zyc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DocumentManager : NSObject

// 创建文件管理
+ (instancetype)ManagerShareInstance;

//获取沙盒路径
- (NSString *)getDocumentPath;

//获取沙盒文件路径
- (NSString *)getDocumentPath:(NSString *)fileName;

//判断沙盒中名为fileName的文件是否存在
-(BOOL) isDocumentFileExists:(NSString *) fileName;

//将工程中已建起的fileName文件写入沙盒
- (void)createDocumentFileName:(NSString *)fileName type:(NSString *)fileType;

//创建fileName文件写入沙盒
-(void) createDocumentFile:(NSString *)fileName;

//获取文件内容
-(NSData *) getContentWithFileName:(NSString *) fileName;
@end
