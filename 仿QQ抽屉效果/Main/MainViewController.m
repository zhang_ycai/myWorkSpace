//
//  MainViewController.m
//  仿QQ抽屉效果
//
//  Created by zhang_ycai on 15-10-13.
//  Copyright (c) 2015年 zyc. All rights reserved.
//

#import "MainViewController.h"
#import "LeftSlideViewController.h"
#import "AFNetworking.h"
#import "BaiDuMapViewController.h"
#import "mianViewController.h"

@interface MainViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *leftTitleArray;
    NSMutableArray *leftControllerArray;
}

@property (nonatomic, strong) UITableView *tableView;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor greenColor];
    /*
    // 左键
    UIButton *leftBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [leftBtn setImage:[UIImage imageNamed:@"usermenu_imgfocus"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * backBtn = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = backBtn;
    //
    self.navigationItem.title = @"畅行南通";
    */
    [self navigationInit];
    
    self.tableView            = [[UITableView alloc] initWithFrame:self.view.bounds];
    self.tableView.delegate   = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
//    NSString *str = [[NSString alloc] init];
    
    [self initArray];
    
    // http请求
}

- (void)navigationInit
{
    self.navigationItem.title = @"首页面";
    UIButton *leftBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [leftBtn setImage:[UIImage imageNamed:@"usermenu_imgfocus"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * backBtn = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = backBtn;
}

#pragma mark - back

#pragma mark - tableViewdelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return leftControllerArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"mainCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
    }
    
    cell.textLabel.text = leftControllerArray[indexPath.row];
    cell.detailTextLabel.text = leftTitleArray [indexPath.row];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UIViewController *viewController = [[NSClassFromString(leftControllerArray[[indexPath row]]) alloc] init];
    if (!viewController) {
        viewController = [[UIViewController alloc] init];
    }
//    BaiDuMapViewController *viewController = [[BaiDuMapViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - LeftSlidedegate
- (void)back
{
    if ([self.delegate respondsToSelector:@selector(leftBtnClicked)]) {
        [self.delegate leftBtnClicked];
    }
}
#pragma mark - 侧边栏对应控制器

- (void)initArray
{
    leftTitleArray = [[NSMutableArray alloc] initWithArray:@[@"百度地图", @"按钮", @"键盘上添加蒙版和按钮", @"无限轮播", @"粒子系统", @"实地圈", @""]];
    leftControllerArray = [[NSMutableArray alloc] initWithArray:@[@"BaiDuMapViewController", @"mianViewController", @"UItextFieldViewController", @"YCScrollowFirstViewController", @"CAEmitterViewController", @"ShiDiQuanViewController", @""]];
}

@end
