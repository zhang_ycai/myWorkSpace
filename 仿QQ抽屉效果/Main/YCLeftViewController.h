//
//  YCLeftViewController.h
//  仿QQ抽屉效果
//
//  Created by zhang_ycai on 15-10-13.
//  Copyright (c) 2015年 zyc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LeftViewControllerdelegate <NSObject>

@optional
- (void)didSelectItem:(NSString *)title;

@end

@interface YCLeftViewController : UIViewController

@property (nonatomic, weak)id<LeftViewControllerdelegate>delegate;

@end
