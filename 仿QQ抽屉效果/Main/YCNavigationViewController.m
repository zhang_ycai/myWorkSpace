//
//  YCNavigationViewController.m
//  仿QQ抽屉效果
//
//  Created by zhang_ycai on 15-10-13.
//  Copyright (c) 2015年 zyc. All rights reserved.
//

#import "YCNavigationViewController.h"

@interface YCNavigationViewController ()

@end

@implementation YCNavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (IOS7_OR_LATER) {
//        [self.navigationBar setBackgroundImage:[UIImage imageNamed:@"表头320-64.png"] forBarMetrics:UIBarMetricsDefault];
    }else
    {
//        [self.navigationBar setBackgroundImage:[UIImage imageNamed:@"表头320-44.png"] forBarMetrics:UIBarMetricsDefault];
    }
    self.navigationBar.backgroundColor = [UIColor greenColor];
    // 统一文字大小和颜色
    NSDictionary *titleAttri = @{NSFontAttributeName:[UIFont systemFontOfSize:22],NSForegroundColorAttributeName:[UIColor whiteColor]};
    [self.navigationBar setTitleTextAttributes:titleAttri];

}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if (self.viewControllers.count == 1) {
        [self.Delegate push];
    }
    [super pushViewController:viewController animated:animated];
}

- (UIViewController *)popViewControllerAnimated:(BOOL)animated
{
    if (self.viewControllers.count == 2) {
        [self.Delegate pop];
    }
    return [super popViewControllerAnimated:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//----------------------->>
/*
 * IOS7之后navigationbar
 * 本来我们的cell是放在（0,0）的位置上的，但是考虑到导航栏、状态栏会挡住后面的主视图，而自动把我们的内容（cell、滚动视图里的元素）向下偏移离Top64px（下方位置如果是tarbar向上偏移离Buttom49px、toolbar是44），也就是当我们把navigationBar给隐藏掉时，滚动视图会给我们的内容预留部分的空白Top（所有内容向下偏移20px，因为状态栏的存在）。
 */

//    self.automaticallyAdjustsScrollViewInsets = NO;//    自动滚动调整，默认为YES
//----------------------->>
/*
 * 这样我们的内容就不会自动偏移了，例如上面的cell就是从（0,0）的位置开始。
 很多人在Nib或者Storyboard中调整视图时，因为IB中有NavigationBar的存在，误导许多人将加入scrollviewB的高度设置为根视图viewA的高度，并且加入的控件、子视图、cell等等都是从viewB的（0,64）开始布局
 */

//默认navigationBar的是半透明，那么它半透明是为了什么？答案是为了可以隐约看到Bar后面的内容，iOS7以上玩多了的人应该有注意到这个问题吧。好了，默认是半透明，看官方的图片，在Bar下方的scrollviewB的frame如果从（0,0）开始，那么我们滚动的内容从即使滚动到Top的位置，还是可以隐隐约约的看到，没问题，挺(T)美(M)的(D)。当我们将Bar设置为不透明

//    self.navigationController.navigationBar.translucent = NO;//    Bar的模糊效果，默认为YES
//----------------------->>
//  开始挖坑！既然不透明了，那么被Bar挡住的视图怎么办，没关系，人家自动帮它们偏移下来，也就是说将你的整个View都往下偏移64px，WTF，我们里面的控件等东东都是一开始布局在view的（0,64）位置的，这样出来不就是整体的效果是在window的（0,128）了？正是如此！也就是我们上面的一开始在（0,0,）位置的scrollviewB现在到了（0,64）的位置，相应的里面的内容也是得到偏移。跟着父视图走嘛！
// 注意此时的self。view的位置也一起下移了

//        if ([[UIDevice currentDevice].systemVersion floatValue]>=7.0) {
//            self.edgesForExtendedLayout = UIRectEdgeNone;
//        }
//----------------------->>
//在iOS 7中，苹果引入了一个新的属性，叫做[UIViewController setEdgesForExtendedLayout:]，它的默认值为UIRectEdgeAll。当你的容器是navigation controller时，默认的布局将从navigation bar的顶部开始。这就是为什么所有的UI元素都往上漂移了44pt。修复这个问题的快速方法就是在方法- (void)viewDidLoad中添加如下一行代码：
//    self.edgesForExtendedLayout = UIRectEdgeNone;
//----------------------->>

@end
