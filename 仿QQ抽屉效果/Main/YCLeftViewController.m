//
//  YCLeftViewController.m
//  仿QQ抽屉效果
//
//  Created by zhang_ycai on 15-10-13.
//  Copyright (c) 2015年 zyc. All rights reserved.
//

#import "YCLeftViewController.h"
#import "ViewController.h"

@interface YCLeftViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *listArray;
@property (nonatomic, strong) NSArray *imageArray;

@end

@implementation YCLeftViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // 隐藏导航栏
    self.navigationController.navigationBar.hidden = YES;
    //
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, (kScreenHeight-350)/2.0, kScreenWidth-100, 350)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    
    self.listArray = @[@"个人中心", @"我的消息", @"意见反馈", @"关于我们", @"离线下载地图", @"帮助", @"分享"];
    self.imageArray = @[@"头像.png", @"信息.png", @"意见反馈.png", @"关于我们.png", @"下载地图.png", @"帮助.png", @"分享.png"];

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.listArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"left";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    cell.textLabel.text = self.listArray[indexPath.row];
    cell.imageView.image = [UIImage imageNamed:self.imageArray[indexPath.row]];
    // 风格设置
    cell.accessoryType =UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([self.delegate respondsToSelector:@selector(didSelectItem:)]) {
        [self.delegate didSelectItem:self.listArray[indexPath.row]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
