//
//  YCNavigationViewController.h
//  仿QQ抽屉效果
//
//  Created by zhang_ycai on 15-10-13.
//  Copyright (c) 2015年 zyc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NavigationVCDelegate <NSObject>

@optional
- (void)push;
- (void)pop;

@end
@interface YCNavigationViewController : UINavigationController
@property (nonatomic, weak) id<NavigationVCDelegate> Delegate;

@end
