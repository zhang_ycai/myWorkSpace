//
//  MainViewController.h
//  仿QQ抽屉效果
//
//  Created by zhang_ycai on 15-10-13.
//  Copyright (c) 2015年 zyc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MainViewControllerdelegate <NSObject>
@optional

- (void)leftBtnClicked;

@end

@interface MainViewController : UIViewController

@property (nonatomic, weak) id<MainViewControllerdelegate> delegate;

@end
